<?php 
include './layout/header.php';
include './lib/functions.php';
?>

<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" data-bs-offset="200"
    class="bg-white position-relative">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Header Section-->
        <div class="m-0">
            <!-- Custom header -->
            <div class="landing-header2 m-0 bg-light " >
            <!--begin::Container-->
            <div class="d-flex flex-row mx-auto align-items-center  ">
                <div class="m-2 "><span class=" text-dark  "><h3><i class="fas fa-2x fa-phone-alt"></i> &nbsp; Call Now: 0722831555</h5></span></div>
                <div class="m-2"><span class=" text-dark  "><h3><i class="fas fa-2x fa-envelope"></i> &nbsp; Email us: info@duarayangu.org</h3></span></div>
            </div>
            <!--end::Container-->
            </div>
            <!--begin::Header-->
            <div class="landing-header mb-13" data-kt-sticky="true" data-kt-sticky-name="landing-header"
                data-kt-sticky-offset="{default: '200px', lg: '300px'}">
                <!--begin::Container-->
                <div class="container-xxl">
                    <!--begin::Wrapper-->
                    <div class="d-flex align-items-center">
                        <!--begin::Logo-->
                        <div class="d-flex align-items-center me-8">
                            <!--begin::Mobile menu toggle-->
                            <button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none ms-n2"
                                id="kt_landing_menu_toggle">
                                <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <path
                                            d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
                                            fill="black" />
                                        <path opacity="0.3"
                                            d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                            <!--end::Mobile menu toggle-->
                            <!--begin::Logo image-->
                            <a href="./index.php">
                                <img alt="Logo" src="./images/dylogo.png" class="h-70px mt-1 pt-0" />
                            </a>
                            <!--end::Logo image-->
                        </div>
                        <!--end::Logo-->
                        <!--begin::Menu wrapper-->
                        <div class="d-lg-block" id="kt_header_nav_wrapper">
                            <div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu"
                                data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                                data-kt-drawer-width="200px" data-kt-drawer-direction="start"
                                data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true"
                                data-kt-swapper-mode="prepend"
                                data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
                                <!--begin::Menu-->
                                <div class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-dark menu-state-title-primary nav nav-flush fs-3 fw-bold"
                                    id="kt_landing_menu">
                                    



                                        <!--end::Trigger-->
                                        <div class="menu-item">
                                            <!--begin::Menu link-->
                                            <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="#home"
                                                data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Home</a>
                                            <!--end::Menu link-->
                                        </div>
                                        <div class="menu-item">
                                            <!--begin::Menu link-->
                                            <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="#work"
                                                data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Events</a>
                                            <!--end::Menu link-->
                                        </div>
                                        <!--end::Menu-->
                                        <!--begin::Menu item-->

                                        <!--begin::Menu item-->



                                
                                  
                                </div>
                                <!--end::Menu-->
                            </div>
                        </div>
                        <!--end::Menu wrapper-->
                        <!--begin::Toolbar-->
                        <div class="flex-equal text-end ms-1 position-relative z-index-2">
                            <a href="#donate" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true" class="btn btn-danger">Donate</a>
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Landing hero-->


            <div class="mb-lg-20">
                <!--begin::Container-->
                <div class="container-xxl position-relative z-index-2">
                    <!--begin::Row-->
                    <div class="row gy-5 align-items-center gx-0">
                        <!--begin::Col-->
                        <div class="col-sm-12 col-md-6">
                            <!--begin::Wrapper-->
                            <div class="pe-0">
                                <!--begin::Title-->
                                <h1 class="text-dark fw-bolder fs-3x fs-lg-4tx mb-14 ms-n1" id="home">We Need Your 
                                <br />Support To
                                <br />Mentor & Educate</h1>
                                <!--end::Title-->
                                <!--begin::Action-->
                                <div class="d-flex flex-shrink-2 flex-wrap me-5">
                                    <!--begin::Link-->
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app" class="btn btn-primary flex-shrink-0 fw-bolder me-2 me-md-4 my-1">Become a mentor</a>
                                    <!--end::Link-->
                                    <!--begin::Link-->
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#kt_modal_new_target"  class="btn btn-warning flex-shrink-0 fw-bolder me-2 me-md-4 my-1">Bring Duara to your School</a>
                                    <!--end::Link-->
                                </div>
                                <!--end::Action-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--End::Col-->
                        <div class="col-sm-12 col-md-6  pe-0">
                            <div class="tns">
                                <div data-tns="true" data-tns-nav-position="bottom" data-tns-controls="false">
                                    <!--begin::Item-->
                                    <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10   overlay overflow-hidden">
                                        <div class="card-body p-0">
                                                <div class="overlay-wrapper">
                                                    <img src="./images/pexels-dazzle-jam-1002061.jpg" class="card-rounded shadow mw-100" alt="" />
                                                </div>
                                            <div class="overlay-layer bg-transparent bg-opacity-0 align-items-end justify-content-center">
                                                    <div class="d-flex flex-grow-1 flex-center  py-5">
                                                    <a href="#donate" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true" class="btn btn-primary btn-shadow">Donate</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Item-->
                                      <!--begin::Item-->
                                      <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10   overlay overflow-hidden">
                                        <div class="card-body p-0">
                                                <div class="overlay-wrapper">
                                                    <img src="./images/pexels-julia-volk-7293100.jpg" class="card-rounded shadow mw-100" alt="" />
                                                </div>
                                            <div class="overlay-layer bg-transparent bg-opacity-0 align-items-end justify-content-center">
                                                    <div class="d-flex flex-grow-1 flex-center  py-5">
                                                        <a href="#donate" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true" class="btn btn-primary btn-shadow">Donate</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Item-->
                                </div>
                            </div>
                        </div>
                        <!--end::Col-->
               


                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Container-->
                <!--begin::Image-->
                <div class="position-absolute top-1 end-0 overflow-hidden w-150px h-450px h-lg-auto w-lg-auto">
                    <img src="../assets/media/svg/layout/1.svg" class="" alt="" />
                </div>
                <!--end::Image-->
            </div>
            <!--end::Landing hero-->
        </div>
        <!--end::Header Section-->
        <!--begin::How It Works Section-->
        <div class="mb-9 pt-15 z-index-2">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Title-->
                <h3 class="fs-lg-2tx fs-2x text-dark text-center mb-16 lh-base" id="work"
                    data-kt-scroll-offset="{default: 100, lg: 150}">
                    How we've already helped <br />
                </h3>
                <!--end::Title-->
                <!--begin::Row-->
				<!-- for loops -->
                <div class="row gy-10 mb-md-20">
                    <!--begin::Col-->
                    <div class="col-sm-12">
                           

<div class="tns tns-default">
    <!--begin::Slider-->
    <div
        data-tns="true"
        data-tns-loop="true"
        data-tns-swipe-angle="false"
        data-tns-speed="2000"
        data-tns-autoplay="true"
        data-tns-autoplay-timeout="18000"
        data-tns-controls="true"
        data-tns-nav="false"
        data-tns-items="1"
        data-tns-center="false"
        data-tns-dots="false"
        data-tns-prev-button="#kt_team_slider_prev1"
        data-tns-next-button="#kt_team_slider_next1">

        <?php
        $conn = connect();
        $n=0;
        $n++;
        $query = mysqli_query($conn, "SELECT * FROM events") or die(mysqli_error());
        while($fetch = mysqli_fetch_array($query)){
        ?>

            <!--begin::Item-->
            <div class="text-center px-5 pt-5 pt-lg-12 px-lg-12   overlay overflow-hidden">
                <div class="card-body p-0">
                        <div class="overlay-wrapper">
                            <img src="./images/<?php echo $fetch['event_image']?>" class="card-rounded shadow mw-100" style="height:400px; width:600px;" alt="" />
                        </div>
                    <div class="overlay-layer bg-transparent bg-opacity-0 align-items-end justify-content-center">
                            <div class="d-flex flex-grow-1 flex-center  py-5">
                                <!-- <p><?php //echo $fetch['event_title'] ?></p> -->
                                <!-- <a href="#" class="btn btn-primary btn-shadow">About  </a> -->
                                <a href="event.php?post=<?php echo $fetch['event_id']?>" class="btn btn-light-primary btn-shadow ms-2">Read more...</a>
                            </div>
                    </div>
                </div>
            </div>
            <!--end::Item-->
            <?php
        }
            ?>
            <!-- <div class="text-center px-5 pt-5 pt-lg-12 px-lg-12  overlay overflow-hidden">
                <div class="card-body p-0">
                        <div class="overlay-wrapper">
                            <img src="./assets/media/stock/600x400/img-3.jpg" class="card-rounded shadow mw-100" alt="" />
                        </div>
                    <div class="overlay-layer bg-transparent bg-opacity-0 align-items-end justify-content-center">
                            <div class="d-flex flex-grow-1 flex-center  py-5">
                                <a href="#" class="btn btn-primary btn-shadow">Join us </a>
                                <a href="#" class="btn btn-light-primary btn-shadow ms-2">Read more...</a>
                            </div>
                    </div>
                </div>
            </div> -->

     
     
        
    
        </div>
    <!--end::Slider-->

    <!--begin::Slider button-->
    <button class="btn btn-icon btn-active-color-primary" id="kt_team_slider_prev1">
        <i class="fas fs-4x fa-chevron-left"></i>
    </button>
    <!--end::Slider button-->

    <!--begin::Slider button-->
    <button class="btn btn-icon btn-active-color-primary" id="kt_team_slider_next1">
    <i class="fas fs-4x fa-chevron-right"></i>
    </button>
    <!--end::Slider button-->
</div>


                    </div>
                    <!--end::Col-->
               
                </div>
                <!--end::Row-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::How It Works Section-->
        <!--begin::Fatures section-->
        <div class="mb-9 pt-15">
            <!--begin::Wrapper-->
                <!--begin::Container-->
                <div class="container-xxl position-relative">
                    <!--begin::Wrapper-->
                    <h3 class="fs-lg-2tx fs-2x text-dark text-center mb-16 lh-base" id="donate"
                    data-kt-scroll-offset="{default: 100, lg: 150}">
                    What are we currently fundraising for?<br />
                </h3>
                
                <div id="kt_content_container" class="container">
                    <!-- Nav pill -->
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            
                         
                          <?php
                            $run_query=mysqli_query($conn,"SELECT * FROM donation");
                            $n=0;
                            
                            while($array=mysqli_fetch_array($run_query)){
                                $id=$array['donation_id'];
                                $title=$array['donation_title'];
                                $n++;
                                 if($n==1){
                                    $show="active";
                                }else{
                                    $show="";
                                }
                                ?>
                                <button class="nav-link <?php echo $show?>" id="nav-section<?php echo $id?>-tab" data-bs-toggle="tab" data-bs-target="#nav-section<?php echo $id?>" type="button" role="tab" aria-controls="nav-section<?php echo $id?>" aria-selected="false"><?php echo $title?></button>
                                <?php
                            }
                            ?>
                            
                           
                          
                         <!--<button class="nav-link" id="nav-section3-tab" data-bs-toggle="tab" data-bs-target="#nav-section3" type="button" role="tab" aria-controls="nav-section3" aria-selected="false">Providing Sanitary Towels</button>-->
                         
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        
                        <?php
                            $run_que=mysqli_query($conn,"SELECT * FROM donation");
                            $n=0;
                            

                            while($array=mysqli_fetch_array($run_que)){
                                $id=$array['donation_id'];
                                $title=$array['donation_title'];
                                $info=$array['donation_info'];
                                $target=$array['donation_target'];
                                $n++;
                                if($n==1){
                                    $show="show active";
                                }else{
                                    $show="";
                                }
                                $p=0;
                                $qe = mysqli_query($conn, "SELECT * FROM transactions WHERE donation_campaign=$id") or die(mysqli_error());
                                while($row = mysqli_fetch_array($qe)){
                                    $amount=$row['amount'];
                                    $p+=$amount;
                                }
                                $progress=$p*100/$target;
                                
                                
                                if($progress<30){
                                    $span='bg-danger';
                                }
                                if($progress>100){
                                    $progress=100;
                                    $span='bg-success';
                                }
                                if($progress>30 && $progress<99){
                                    $span='bg-primary';
                                }
                                ?>
                        <div class="tab-pane fade <?php echo $show;?> " id="nav-section<?php echo $id?>" role="tabpanel" aria-labelledby="nav-section<?php echo $id?>-tab">
                            <!--begin::Row-->
							<div class="row g-5 g-xxl-10 mt-5">
								<!--begin::Col-->
								<div class="col-xl-6 col-xxl-6 mb-5 mb-xxl-10">
									<!--begin::Engage widget 2-->
									<div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px h-xl-100 bg-gray-200 border-0" style="background-position: 100% 100%;background-size: 500px auto">
										<!--begin::Body-->
										<div class="card-body d-flex flex-column justify-content-center ps-lg-15">
											<!--begin::Title-->
											<h3 class="text-gray-800 fs-2qx fw-boldest mb-4 mb-lg-8">
											    <?php echo $info?>
                                            
											<br />Donate!</h3>
											<!--end::Title-->
										</div>
										<!--end::Body-->
									</div>
									<!--end::Engage widget 2-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-xl-6 col-xxl-6 col-sm-12 mb-xl-5 mb-xxl-10 mx-xs-4">
									<!--begin::Card widget 4-->
									<h3 class="text-gray-800 fs-2qx fw-boldest mb-4 mb-lg-8">
                                        Our Goal.
                                        <br />Ksh <?php echo $array['donation_target']?> /= 
                                    <!-- <br />Donate! --></h3>
                                    <div id="kt_slider_basic">
                                        <div class="progress mb-5">
                                            <div class="progress-bar <?php echo $span?>" role="progressbar" style="width: <?php echo $progress?>%;" aria-valuenow="<?php echo $progress?>"
                                                aria-valuemin="0" aria-valuemax="100"><?php echo $progress?>%</div>
                                        </div>
                                    </div>
                                        <form id="support_girl" class="form" method ='POST' action="formprocessor.php" autocomplete="off">
                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10">
                                                <label class="required fw-bold fs-6 mb-2">Select Donation Amount</label>
                                                <div class="row mb-2" data-kt-buttons="true">
                                                <div class="col">
													<!--begin::Option-->
													<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">
														<input type="radio" class="btn-check" name="cardamount" value="1000" id="kt_docs_formvalidation_radio_option_1" />
														<span class="fw-bolder fs-3">1000 KSH</span>
													</label>
													<!--end::Option-->
												</div>
                                                <div class="col">
													<!--begin::Option-->
													<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">
														<input type="radio" class="btn-check" name="cardamount" value="5000" id="kt_docs_formvalidation_radio_option_2" />
														<span class="fw-bolder fs-3">5000 KSH</span>
													</label>
													<!--end::Option-->
												</div>
                                                <div class="col">
													<!--begin::Option-->
													<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">
														<input type="radio" class="btn-check" name="cardamount" value="15000" id="kt_docs_formvalidation_radio_option_3" />
														<span class="fw-bolder fs-3">15000 KSH</span>
													</label>
													<!--end::Option-->
												</div>
                                                <div class="col">
													<!--begin::Option-->
													<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">
														<input type="radio" class="btn-check" name="cardamount" value="Other" id="kt_docs_formvalidation_radio_option_4" />
														<span class="fw-bolder fs-3">Other</span>
													</label>
													<!--end::Option-->
												</div>
                                                </div>
                                            </div>
                                      
                                                    <!--begin::Input group-->
                                                    <div class="fv-row mb-10">
                                                        <label class="required fw-bold fs-6 mb-2">Choose Mode of Payment</label>
                                                        <div class="row mb-2" data-kt-buttons="true">
                                                            <div class="col-lg-4">
                                                                <!--begin::Option-->
                                                                <label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10">
                                                                    <input type="radio" class="btn-check" name="payment_method" value="Visa" />
                                                                    <span class="svg-icon svg-icon-3x me-5">
                                                                        <img src="./images/SeekPng.com_makka-png_3727055.png" alt="" class="h-25px" />
                                                                    </span>
                                                                </label>
                                                                <!--end::Option-->
                                                            </div>
                                                            <div class="col-lg-4">
                                                                        <!--begin::Option-->
                                                                        <label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10">
                                                                            <input type="radio" class="btn-check" name="payment_method" value="Paypal" />
                                                                            <span class="svg-icon svg-icon-3x me-5">
                                                                                <img src="./images/SeekPng.com_paypal-png_185650.png" alt="" class="h-25px" />
                                                                            </span>
                                                                        </label>
                                                                <!--begin::Option-->
                                                                                                                     <!--end::Option-->
                                                            </div>
                                                            <div class="col-lg-4">
                                                                                                                                <!--begin::Option-->
                                                                        <label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10">
                                                                            <input type="radio" class="btn-check" name="payment_method" value="Mpesa" />
                                                                            <span class="svg-icon svg-icon-3x me-5">
                                                                                <img src="./images/SeekPng.com_paypal-icon-png_9498299.png" alt="" class="h-25px" />
                                                                            </span>
                                                                        </label>
                                                                        <!--end::Option-->
                                                            </div>
                                                     
                                                        </div>
                                                    </div>
                                                    <!--begin::Input group-->
                                                    <button id="support_girl2" type="submit" name="support_girl2" class="btn btn-primary">
                                                        <span class="indicator-label">
                                                           Donate
                                                        </span>
                                                        <span class="indicator-progress">
                                                            Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                        </span>
                                                    </button>
                                         
                                        <!-- <button type="submit" class="btn btn-primary" name="train_mentors">Donate</button> -->
                                     </form>

									<!--end::Card widget 4-->
								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
                        </div>
                        <?php
                            }
                            ?>
       <!--                 <div class="tab-pane fade" id="nav-section3" role="tabpanel" aria-labelledby="nav-section3-tab">-->
                            <!--begin::Row-->
							<!--<div class="row g-5 g-xxl-10 mt-5">-->
								<!--begin::Col-->
							<!--	<div class="col-xl-6 col-xxl-6 mb-5 mb-xxl-10">-->
									<!--begin::Engage widget 2-->
							<!--		<div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px h-xl-100 bg-gray-200 border-0" style="background-position: 100% 100%;background-size: 500px auto;background-image:url()">-->
										<!--begin::Body-->
							<!--			<div class="card-body d-flex flex-column justify-content-center ps-lg-15">-->
											<!--begin::Title-->
							<!--				<h3 class="text-gray-800 fs-2qx fw-boldest mb-4 mb-lg-8">-->
       <!--                                         Lorem Ipsum is simply dummy text of the printing and typesetting industry.-->
       <!--                                         <br />Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, -->
       <!--                                         <br />when an unknown printer took a galley of type and scrambled it to make a type specimen book. -->
                                            
							<!--				<br />Donate!</h3>-->
											<!--end::Title-->
							<!--			</div>-->
										<!--end::Body-->
							<!--		</div>-->
									<!--end::Engage widget 2-->
							<!--	</div>-->
								<!--end::Col-->
								<!--begin::Col-->
							<!--	<div class="col-xl-6 col-xxl-6 col-sm-12 mb-xl-5 mb-xxl-10 mx-xs-4">-->
									<!--begin::Card widget 4-->
							<!--		<h3 class="text-gray-800 fs-2qx fw-boldest mb-4 mb-lg-8">-->
       <!--                                 Our Goal.-->
       <!--                                 <br />Ksh 2,000,000 /= -->
                                    <!-- <br />Donate! --></h3>
       <!--                             <div id="kt_slider_basic">-->
       <!--                                 <div class="progress mb-5">-->
       <!--                                     <div class="progress-bar" role="progressbar" style="width: 85%;" aria-valuenow="85"-->
       <!--                                         aria-valuemin="0" aria-valuemax="100">85%</div>-->
       <!--                                 </div>-->
       <!--                             </div>-->
       <!--                                 <form id="kt_docs_formvalidation_text" class="form" action="#" name="form3" autocomplete="off">-->
                                            <!--begin::Input group-->
       <!--                                     <div class="fv-row mb-10">-->
       <!--                                         <label class="required fw-bold fs-6 mb-2">Select Donation Amount</label>-->
       <!--                                         <div class="row mb-2" data-kt-buttons="true">-->
       <!--                                         <div class="col">-->
													<!--begin::Option-->
							<!--						<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">-->
							<!--							<input type="radio" class="btn-check" name="account_team_size" value="50+" />-->
							<!--							<span class="fw-bolder fs-3">1000 KSH</span>-->
							<!--						</label>-->
													<!--end::Option-->
							<!--					</div>-->
       <!--                                         <div class="col">-->
													<!--begin::Option-->
							<!--						<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">-->
							<!--							<input type="radio" class="btn-check" name="account_team_size" value="50+" />-->
							<!--							<span class="fw-bolder fs-3">5000 KSH</span>-->
							<!--						</label>-->
													<!--end::Option-->
							<!--					</div>-->
       <!--                                         <div class="col">-->
													<!--begin::Option-->
							<!--						<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">-->
							<!--							<input type="radio" class="btn-check" name="account_team_size" value="50+" />-->
							<!--							<span class="fw-bolder fs-3">15000 KSH</span>-->
							<!--						</label>-->
													<!--end::Option-->
							<!--					</div>-->
       <!--                                         <div class="col">-->
													<!--begin::Option-->
							<!--						<label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4">-->
							<!--							<input type="radio" class="btn-check" name="account_team_size" value="50+" />-->
							<!--							<span class="fw-bolder fs-3">25000 KSH</span>-->
							<!--						</label>-->
													<!--end::Option-->
							<!--					</div>-->
       <!--                                         </div>-->
       <!--                                     </div>-->
                                             <!--begin::Input group-->
                                                    <!--begin::Input group-->
       <!--                                             <div class="fv-row mb-10">-->
       <!--                                                 <label class="required fw-bold fs-6 mb-2">Choose Mode of Payment</label>-->
       <!--                                                 <div class="row mb-2" data-kt-buttons="true">-->
       <!--                                                     <div class="col-lg-4">-->
                                                                <!--begin::Option-->
       <!--                                                         <label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10">-->
       <!--                                                             <input type="radio" class="btn-check" name="payment_method" value="Visa" />-->
       <!--                                                             <span class="svg-icon svg-icon-3x me-5">-->
       <!--                                                                 <img src="./images/SeekPng.com_makka-png_3727055.png" alt="" class="h-25px" />-->
       <!--                                                             </span>-->
       <!--                                                         </label>-->
                                                                <!--end::Option-->
       <!--                                                     </div>-->
       <!--                                                     <div class="col-lg-4">-->
                                                                        <!--begin::Option-->
       <!--                                                                 <label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10">-->
       <!--                                                                     <input type="radio" class="btn-check" name="payment_method" value="Paypal" />-->
       <!--                                                                     <span class="svg-icon svg-icon-3x me-5">-->
       <!--                                                                         <img src="./images/SeekPng.com_paypal-png_185650.png" alt="" class="h-25px" />-->
       <!--                                                                     </span>-->
       <!--                                                                 </label>-->
                                                                <!--begin::Option-->
                                                                                                                     <!--end::Option-->
       <!--                                                     </div>-->
       <!--                                                     <div class="col-lg-4">-->
                                                                                                                                <!--begin::Option-->
       <!--                                                                 <label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10">-->
       <!--                                                                     <input type="radio" class="btn-check" name="payment_method" value="Mpesa" />-->
       <!--                                                                     <span class="svg-icon svg-icon-3x me-5">-->
       <!--                                                                         <img src="./images/SeekPng.com_paypal-icon-png_9498299.png" alt="" class="h-25px" />-->
       <!--                                                                     </span>-->
       <!--                                                                 </label>-->
                                                                        <!--end::Option-->
       <!--                                                     </div>-->
                                                     
       <!--                                                 </div>-->
       <!--                                             </div>-->
                                                    <!--begin::Input group-->
                                         
       <!--                                 <button type="button" class="btn btn-primary">Donate</button>-->
       <!--                              </form>-->

									<!--end::Card widget 4-->
							<!--	</div>-->
								<!--end::Col-->
							<!--</div>-->
							<!--end::Row-->
       <!--                 </div>-->
                    </div>

                </div>
                    <!--end::Wrapper-->
                   
                </div>
                <!-- break -->
                <div class="container-xxl mb-7 pt-15 mx-xxs-4">
                    <div class="card border-0 bg-primary overflow-hidden mb-lg-15 z-index-2">
                        <!--begin::Body-->
                        <div class="card-body text-center my-9">
                            <!--begin::Title-->
                            <div
                                class="fs-lg-3tx fs-2x fw-bolder text-white pt-lg-20 mt-lg-4 mb-lg-20 mb-10 pb-2 position-relative z-index-2">
                                <span class="d-block mb-8">It's not how much we give but how much love we put into giving.</span>
                                <span class="d-block lh-0">-Mother Teresa</span>
                            </div>
                            <!--end::Title-->
                            <!--begin::Action-->
                            <!-- <div class="mb-lg-20 position-relative z-index-2">
                                <a href="./Donate.html" class="btn btn-danger btn-lg h-55px pt-5">Donate</a>
                            </div> -->
                            <!--end::Action-->
                            <img src="assets/media/product/3.svg" class="position-absolute bottom-0 end-0" alt="" />
                        </div>
                        <!--end::Body-->
                    </div>
            </div>
            <!-- section 2 -->
         
            <!--begin::Visual section-->
    
            <!--end::Visual section-->
            <!--end::Wrapper-->
        </div>
        <div class="card border-0 bg-transparent text-center">
            <h3 class="fs-lg-2tx fs-2x text-dark text-center mb-7 mb-lg-16 lh-base position-relative z-index-2"
            id="fatures" data-kt-scroll-offset="{default: 100, lg: 150}">
           Our Partners
            </h3>
            <!--begin::Body-->
            <div class="card-body py-8">
                <!--begin::Icon-->
                <a href="#" class="mx-5">
                    <i class="fab fa-6x fa-dhl"></i>
                </a>
                <!--end::Icon-->
                <!--begin::Icon-->
                <a href="#" class="mx-5">
                    <i class="fab fa-6x fa-angrycreative"></i>
                </a>
                <!--end::Icon-->
                <!--begin::Icon-->
                <a href="#" class="mx-5">
                    <i class="fab fa-6x fa-node"></i>
                </a>
                <!--end::Icon-->
                <!--begin::Icon-->
                <!-- <a href="#" class="mx-4">
                    <img src="../assets/media/svg/brand-logos/behance.svg" class="h-30px my-2" alt="" />
                </a> -->
                <!--end::Icon-->
                <!--begin::Icon-->
                <!-- <a href="#" class="mx-4">
                    <img src="../assets/media/svg/brand-logos/pinterest-p.svg" class="h-30px my-2" alt="" />
                </a> -->
                <!--end::Icon-->
                <!--begin::Icon-->
                <a href="#" class="mx-5">
                    <i class="fab fa-6x fa-fedex"></i>
                </a>
                <!--end::Icon-->
                <!--begin::Icon-->
                <!-- <a href="#" class="mx-4">
                    <img src="../assets/media/svg/brand-logos/dribbble-icon-1.svg" class="h-30px my-2" alt="" />
                </a> -->
                <!--end::Icon-->
            </div>
            <!--end::Body-->
        </div>
<!-- modals -->
<!--begin::Modal - Become a mentor modal-->
<div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Modal title-->
						<h2>Become a mentor application Form</h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin::Stepper-->
						<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
							<!--begin::Aside-->
							<div class="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
								<!--begin::Nav-->
								<div class="stepper-nav ps-lg-10">
									<!--begin::Step 1-->
									<div class="stepper-item current" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">1</span>
										</div>
										<!--end::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Details</h3>
											<div class="stepper-desc">Your Bio Data</div>
										</div>
										<!--end::Label-->
									</div>
									<!--end::Step 1-->
									<!--begin::Step 2-->
									<div class="stepper-item" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">2</span>
										</div>
										<!--begin::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Submit your CV</h3>
											<div class="stepper-desc">Document submission</div>
										</div>
										<!--begin::Label-->
									</div>
									<!--end::Step 2-->
				
									
									<!--begin::Step 5-->
									<div class="stepper-item" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">3</span>
										</div>
										<!--end::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Completed</h3>
											<div class="stepper-desc">Review and Submit</div>
										</div>
										<!--end::Label-->
									</div>
									<!--end::Step 5-->
								</div>
								<!--end::Nav-->
							</div>
							<!--begin::Aside-->
							<!--begin::Content-->
							<div class="flex-row-fluid py-lg-5 px-lg-15">
								<!--begin::Form-->
								<form class="form"  id="kt_modal_create_app_form" action="formprocessor.php" method="post"  enctype="multipart/form-data">
									<!--begin::Step 1-->
									<div class="current" data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="row fv-row ">
                                                <div class="col-6">
                                                    <!--begin::Label-->
                                                    <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                        <span class="required">First Name</span>
                                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your First name as it appears in your ID"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <input type="text" class="form-control form-control-lg form-control-solid" name="fname" placeholder="" value="" />
                                                </div>
                                                <div class="col-6">
                                                    <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                        <span class="required">Last Name</span>
                                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your Last name as it appears in your ID"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <input type="text" class="form-control form-control-lg form-control-solid" name="lname" placeholder="" value="" />
                                                    <!--end::Input-->
                                                </div>
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="fv-row">
												<!--begin::Repeater-->
												<div id="kt_docs_repeater_basic">
													<!--begin::Form group-->
													<div class="form-group">
														<div data-repeater-list="kt_docs_repeater_basic">
															<div data-repeater-item>
																<div class="form-group row">
																	<div class="col-8">
																		<label class="d-flex align-items-center fs-5 fw-bold mb-2">
																			<span class="required">Mobile phone</span>
																			<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your Mobile number for contact."></i>
																		</label>
																		<div class="input-group mb-3">
																			<input type="text" class="form-control" name ="mobilephone" placeholder="Enter mobile number" />
																		
																		</div>
																	
																	</div> 
																	<div class="col-4">
																		<a href="javascript:;" data-repeater-delete class="btn btn-sm btn-light-danger mt-3 mt-md-8">
																			<i class="fas fa-trash"></i>Delete
																		</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!--end::Form group-->

													<!--begin::Form group-->
													<div class="form-group mt-1 mb-4">
														<a href="javascript:;" data-repeater-create class="btn btn-sm btn-light-primary">
															<i class="fas fa-plus"></i></i>Add another Number
														</a>
													</div>
													<!--end::Form group-->
												</div>
												<!--end::Repeater-->								
											</div>
											<!--end::Input group-->
                                            	<!--begin::Input group-->
											<div class="fv-row">
                                                <!--begin::Label-->
                                                   <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                       <span class="required">Location</span>
                                                       <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your location for easy reach."></i>
                                                   </label>
                                                   <!--end::Label-->
                                                   <!--begin::Input-->
                                                   <select class="form-select form-select-solid" data-dropdown-parent="#kt_modal_create_app_form" data-control="select2" data-placeholder="Select an option" name="location">
                                                    <option></option>
                                                    <option value="Coast_Region">Coast Region</option>
                                                    <option value="Eastern_Region">Eastern Region</option>
                                                    <option value="NorthEastern_Region">NorthEastern Region</option>
                                                    <option value="Central_Region">Central Region</option>
                                                    <option value="RiftValley_Region">RiftValley Region</option>
                                                    <option value="Western_Region">Western Region</option>
                                                    <option value="Nyanza_Region">Mombasa</option>
                                                    <option value="Nairobi">Nairobi</option>
                                                </select>
                                           
                                            </div>
                                               <!--end::Input group-->
                                                	<!--begin::Input group-->
											<div class="fv-row">
                                                <!--begin::Label-->
                                                   <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                       <span class="required">Email</span>
                                                       <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your unique email for contact"></i>
                                                   </label>
                                                   <!--end::Label-->
                                                   <!--begin::Input-->
                                                   <input type="text" class="form-control form-control-lg form-control-solid" name="email" placeholder="" value="" />
                                           
                                            </div>
                                               <!--end::Input group-->
                                               <div class="fv-row">
                                                    <!--begin::Label-->
                                                    <label class="form-label">
                                                    <span class="required" >Describe yourself</span>
                                                    <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Describe yourself briefly"></i>
                                                    </label>
                                                    <!--end::Label-->
                                                    <input class="form-control mb-2" name="info">

                                                    <!--begin::Editor-->
                                                    <!-- <div id="kt_ecommerce_add_product_description" name="kt_ecommerce_add_product_description" class="min-h-200px mb-2"></div> -->
                                                    <!--end::Editor-->
                                            </div>
										</div>
									</div>
									<!--end::Step 1-->
									<!--begin::Step 2-->
									<div data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="fv-row">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-4">
													<span class="required">Upload your CV</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Upload your current CV document"></i>
												</label>
												<!--end::Label-->
												
											</div>
											<!--end::Input group-->
											 <!--begin::Input group-->
											<div class="fv-row">
												<!--begin::Dropzone-->
												
                                                    <input  class="dropzone" type="file" name="fileToUpload" id="fileToUpload">
                                                    <div class="form-text">Allowed file types: pdf, docx, doc.</div>
												<!--end::Dropzone-->
											</div>
											<!--end::Input group-->
											
										</div>
									</div>
									<!--end::Step 2-->
								
									<!--begin::Step 3-->
									<div data-kt-stepper-element="content">
										<div class="w-100 text-center">
											<!--begin::Heading-->
											<h1 class="fw-bolder text-dark mb-3">Submit</h1>
											<!--end::Heading-->
											<!--begin::Description-->
											<div class="text-muted fw-bold fs-3">The details you have provided will be submitted when you click submit.Ensure you have accurately submitted your details and CV correctly.If not,you can click back and review.</div>
											<!--end::Description-->
											<!--begin::Illustration-->
											<div class="text-center px-4 py-15">
												<img src="images/dylogo.png" alt="" class="mw-100 mh-300px" />
											</div>
											<!--end::Illustration-->
										</div>
									</div>
									<!--end::Step 3s-->
									<!--begin::Actions-->
									<div class="d-flex flex-stack pt-10">
										<!--begin::Wrapper-->
										<div class="me-2">
											<button type="button" class="btn btn-lg btn-light-primary me-3" data-kt-stepper-action="previous">
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
											<span class="svg-icon svg-icon-3 me-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
													<path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->Back</button>
										</div>
										<!--end::Wrapper-->
										<!--begin::Wrapper-->
										<div>
											<button type="submit" name="becomeamentor" class="btn btn-lg btn-primary" data-kt-stepper-action="submit">
												<span class="indicator-label">Submit 
												<!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
												<span class="svg-icon svg-icon-3 ms-2 me-0">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
														<path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon--></span>
												<span class="indicator-progress">Please wait... 
												<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
											</button>
											<button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">Continue 
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
											<span class="svg-icon svg-icon-3 ms-1 me-0">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
													<path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon--></button>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Actions-->
								</form>
								<!--end::Form-->
							</div>
							<!--end::Content-->
						</div>
						<!--end::Stepper-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
		<!--end::Modal - Create App-->
        	<!--begin::Modal - New Target-->
		<div class="modal fade" id="kt_modal_new_target" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-650px">
				<!--begin::Modal content-->
				<div class="modal-content rounded">
					<!--begin::Modal header-->
					<div class="modal-header pb-0 border-0 justify-content-end">
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--begin::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
						<!--begin:Form-->
						<form id="kt_modal_new_target_form" class="form" action="formprocessor.php" method="POST">
							<!--begin::Heading-->
							<div class="mb-13 text-center">
								<!--begin::Title-->
								<h1 class="mb-3">Bring us to your school</h1>
								<!--end::Title-->
								<!--begin::Description-->
								<div class="text-muted fw-bold fs-5">If you need more info, please check our
								<a href="#" class="fw-bolder link-primary">Guidelines</a>.</div>
								<!--end::Description-->
							</div>
							<!--end::Heading-->
							<!--begin::Input group-->
							<div class="row g-9 mb-8">
									<div class="col-6 fv-row">
										<!--begin::Label-->
										<label class="d-flex align-items-center fs-6 fw-bold mb-2">
											<span class="required">School Name</span>
											<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference"></i>
										</label>
										<!--end::Label-->
										<input type="text" class="form-control form-control-solid" placeholder="Enter your School name" name="school_name" />
									</div>
									<!--begin::Col-->
									<div class="col-6 fv-row">
										<label class="required fs-6 fw-bold mb-2">School Location</label>
										<select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Select your School's Location" name="school_location">
											<option ></option>
											<option value="Westlands">Westlands</option>
											<option value="Dagoretti_North">Dagoretti North</option>
											<option value="Dagoretti_South">Dagorreti South</option>
											<option value="Langata">Langata</option>
											<option value="Kibra">Kibra</option>
											<option value="Roysambu">Roysambu</option>
											<option value="Kasarani">Kasarani</option>
											<option value="Ruaraka">Ruaraka</option>
											<option value="Embakasi South">Embakasi South</option>
											<option value="Embakasi North">Embakasi North</option>
											<option value="Embakasi Central">Embakasi Central</option>
											<option value="Embakasi East">Embakasi East</option>
											<option value="Embakasi West">Embakasi West</option>
											<option value="Makadara">Makadara</option>
											<option value="Kamkunji">Kamkunji</option>
											<option value="Starehe">Starehe</option>
											<option value="Mathare">Mathare</option>
										
										</select>
									</div>
									<!--end::Col-->
							</div>
							<!--end::Input group-->
							<!--begin::Input group-->
							<div class="row g-9 mb-8">
									<div class="col-6 fv-row">
										<!--begin::Label-->
										<label class="d-flex align-items-center fs-6 fw-bold mb-2">
											<span class="required">School Email</span>
											<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference"></i>
										</label>
										<!--end::Label-->
										<input type="text" class="form-control form-control-solid" placeholder="Enter your School name" name="school_email" />
									</div>
									<!--begin::Col-->
									<div class="col-6 fv-row">
										<label class="d-flex align-items-center fs-6 fw-bold mb-2">
											<span class="required">School mobile number</span>
											<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference"></i>
										</label>
										<!--end::Label-->
										<input type="text" class="form-control form-control-solid" placeholder="Enter your School's mobile number" name="school_mobile" />
									</div>
									<!--end::Col-->
							</div>
							<!--end::Input group-->
							<!--begin::Input group-->
							<div class="row g-9 mb-8">
									<!--begin::Col-->
									<div class="col-6 fv-row">
										<label class="d-flex align-items-center fs-6 fw-bold mb-2">
											<span class="required">School principal's name</span>
											<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference"></i>
										</label>
										<!--end::Label-->
										<input type="text" class="form-control form-control-solid" placeholder="Enter your School principal's name" name="school_principal_name" />
										</select>
									</div>
									<!--end::Col-->
										<!--begin::Col-->
										<div class="col-6 fv-row">
											<label class="d-flex align-items-center fs-6 fw-bold mb-2">
												<span class="required">Princpal's mobile number</span>
												<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference"></i>
											</label>
											<!--end::Label-->
											<input type="text" class="form-control form-control-solid" placeholder="Enter your School principal's mobile number" name="school_principal_number" />
                                            <input type="text" hidden="true" class="form-control form-control-solid" placeholder="bringustoyourschool2" name="bringustoyourschool2" />
											</select>
										</div>
										<!--end::Col-->
							</div>
							<!--end::Input group-->
							<!--begin::Actions-->
							<div class="text-center">
								<button type="reset" id="kt_modal_new_target_cancel" class="btn btn-light me-3">Cancel</button>
								<button type="submit" id="kt_modal_new_target_submit" class="btn btn-primary" name="bringustoyourschool">
									<span class="indicator-label">Submit</span>
									<span class="indicator-progress">Please wait... 
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
								</button>
							</div>
							<!--end::Actions-->
						</form>
						<!--end:Form-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
		<!--end::Modal - New Target-->
        <!-- Toast for the page when become a mentor is successful -->

        

<!--begin::Button-->
<!-- <button type="button" class="btn btn-primary" id="kt_docs_toast_stack_button">Toggle Toast</button> -->
<!--end::Button-->

<!--begin::Success Toast for become and School-->
<div id="mentor_success" class="toast-container position-fixed top-1 end-0 p-3 z-index-3">
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-kt-docs-toast="stack">
        <div class="toast-header">
            <i class="far fa-lightbulb text-success fs-3"></i>
            <strong class="me-auto">&nbsp;&nbsp;Duara yangu Notification</strong>
            <small>Just now</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Application successful. Be on the lookout for our Email.
            Thank you!
        </div>
    </div>
</div>
<!--end::Toast-->

<!--begin::Failed Toast for become-->
<div id="toast_failed" class="toast-container position-fixed top-1 end-0 p-3 z-index-3">
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-kt-docs-toast="stack">
        <div class="toast-header">
            <i class="far fa-times-circle text-danger fs-3"></i>
            <!-- <i class="far fa-lightbulb text-success fs-3"></i> -->
            <strong class="me-auto">&nbsp;&nbsp;Duara yangu Notification</strong>
            <small>Just now</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Application Failed. Try again or contact us! 
         
        </div>
    </div>
</div>
<!--end::Toast-->



<?php
include './layout/footer.php';
?>