<?php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>Document Notification</title>

    <style>

		html, body {

			padding: 0; margin:0; 
		}

        }

    </style>

</head>

<body style="
font-family:Arial,Helvetica,sans-serif; 
line-height: 1.5; 
font-weight: normal; 
font-size: 15px; 
color: #2F3044; 
min-height: 100%; 
margin:0; 
padding:0; 
width:100%; 
background-color:#edf2f7
">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0 auto; padding:0; max-width:600px">

			<tr>
				<td align="center" valign="center" style="text-align:center; padding: 40px">
					<a href="https://www.duarayangu.org" rel="noopener" target="_blank">
						<img alt="Duara yangu"  width="45%" style="max-width:250px;"  src="http://drive.google.com/uc?export=view&id=1DZ9Q1TPFd9_ccbwCAqvYeG-priCpnuWS" />
					</a>
				</td>
			</tr>
			<tr>
				<td align="left" valign="center">
					<div style="text-align:left; margin: 0 20px; padding: 40px; background-color:#ffffff; border-radius: 6px">
						<!--begin:Email content-->
						<div style="padding-bottom: 30px; font-size: 17px;">
							<strong>Dear ${user-name},</strong>
						</div>
						<div style="padding-bottom: 30px">Thank you very much for applying for the Mentorship position at Duara Yangu. 
						</div>
						<div style="padding-bottom: 30px">We've received your application and we will review it in the next couple of days. If you are selected for the next phase of the recruitment process, you will be contacted for an interview session.</div>
						<div style="padding-bottom: 30px">We appreciate your interest in our company and wish you the very best in this selection process.</div>
						
						<!--end:Email content-->
						<div style="padding-bottom: 10px">Kind regards,
						<br>The Duara Yangu Team. 
						<tr>
							<td align="center" valign="center" style="font-size: 13px; text-align:center;padding: 20px; color: #6d6e7c;">
								<p>Nairobi, KENYA.</p>
								<p>Copyright © 
								<a href="https://www.duarayangu.org" rel="noopener" target="_blank">DuaraYangu</a>.</p>
							</td>
						</tr></br></div>
					</div>
				</td>
			</tr>
	

	</table>

</body>

</html>

