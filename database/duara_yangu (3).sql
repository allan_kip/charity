-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2022 at 08:04 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `duara_yangu`
--

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `donation_id` int(11) NOT NULL,
  `donation_title` varchar(255) NOT NULL,
  `donation_info` varchar(2000) NOT NULL,
  `donation_target` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  `donation_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`donation_id`, `donation_title`, `donation_info`, `donation_target`, `created_on`, `donation_image`, `status`) VALUES
(1, 'first trial', 'first donation\r\ntrial             ', '3000', '2022-02-09', 'WhatsApp Image 2022-01-25 at 15.29.42 (2).jpeg', 1),
(2, 'Training Mentors & 30129', 'Lets join together and make girls aware and equipped with necessary knowledge.\r\n                                            ', '2000', '2022-02-14', '1642120672258.png', 1),
(3, 'Training Mentors & ambassadors', 'Lets join together and make girls aware and equipped with necessary knowledge.\r\nLEts look forward always\r\n                                            ', '2000000', '2022-02-14', 'WhatsApp Image 2022-01-25 at 15.29.42 (2).jpeg', 1),
(4, 'Sanitary towels', 'Lets join hands\r\n                                            ', '100000', '2022-02-14', 'WhatsApp Image 2022-02-11 at 10.31.59 PM.jpeg', 5),
(5, 'support a girls sacco', 'loremloroeoripjijrprijpioipim\r\nvkmwpvmw\r\n                                            ', '200000', '2022-02-14', 'IMG-20220114-WA0000.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_info` varchar(2000) NOT NULL,
  `event_image` varchar(255) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(255) NOT NULL,
  `member_email` varchar(255) NOT NULL,
  `member_tel` varchar(255) NOT NULL,
  `member_pass` varchar(255) NOT NULL,
  `member_address` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  `member_type` int(11) NOT NULL,
  `member_doc` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `member_info` varchar(255) NOT NULL,
  `school_principal` varchar(255) NOT NULL,
  `principal_tel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `member_name`, `member_email`, `member_tel`, `member_pass`, `member_address`, `created_on`, `member_type`, `member_doc`, `status`, `member_info`, `school_principal`, `principal_tel`) VALUES
(1, 'adminadmin', 'admin@admin.com', '', '12345678', '', '0000-00-00', 1, '', 0, '', '', ''),
(2, 'Allankip', 'kipallan7@gmail.com', '0726002003', '', 'Nairobi', '2022-02-14', 5, '', 0, '', '', ''),
(4, 'Muranga uni', 'mut.ac.ke', '07712312313', '', 'Muranga', '2022-02-12', 10, '', 0, '', '', ''),
(5, 'AllanKirui', 'keiyoh@gmail.com', '', '', 'Kenya', '0000-00-00', 5, 'assets/documents/7b4529f6f0dbc365fded0e023a4fc506', 0, '', '', ''),
(6, 'AllanKirui', 'kipallan@gmail.com', '', '', 'Kenya', '2022-02-24', 5, 'assets/documents/49136eddebd110513d00c45196e40489', 0, 'I am a motivated speaker with the urge of pursuing and teaching others about life\r\n                                                ', '', ''),
(7, 'AllanKirui', 'kipallan70@gmail.com', '', '', 'Kenya', '2022-02-24', 5, 'assets/documents/4495dda8e01577b5160e447753539b33', 0, 'Capable of become a good mentor\r\n                                                ', '', ''),
(8, 'AllanKirui', 'indigoregime@gmail.com', '', '', 'Kenya', '2022-02-24', 5, 'assets/documents/6cbbef928ad1ef525b2eeddf0ff6c93e', 0, 'Very active mentor with years of expreicnce\r\n                                                ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mobilenumbers`
--

CREATE TABLE `mobilenumbers` (
  `addressID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL,
  `phone_number` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mobilenumbers`
--

INSERT INTO `mobilenumbers` (`addressID`, `memberID`, `phone_number`) VALUES
(1, 7, 726002003),
(2, 7, 2147483647),
(3, 7, 726002002),
(4, 8, 726002000),
(5, 8, 726002001),
(6, 8, 726002005);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `trans_id` int(11) NOT NULL,
  `transactionId` varchar(255) NOT NULL,
  `trans_member` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `donation_campaign` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`trans_id`, `transactionId`, `trans_member`, `payment_type`, `amount`, `status`, `trans_date`, `donation_campaign`) VALUES
(1, 'MKAI10NOD1E ', 'Allan', 1, '1000', 1, '2022-02-14', 4),
(2, 'MKAI10NOD1E ', 'Moses ', 5, '10000', 5, '2022-02-14', 5),
(3, 'MKAI10NOD1qwq', 'Allan Kirui', 10, '2000', 5, '2022-02-24', 3),
(4, 'PMNDFSsdfw', 'kipallan@gmail.com', 10, '4000', 5, '2022-02-24', 2),
(5, '12jnjncowni', 'edgarwab', 10, '1700', 5, '2022-02-24', 2),
(6, 'MKAI10NOD1E ', 'Allan Kirui', 5, '1000', 5, '2022-02-24', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`donation_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `mobilenumbers`
--
ALTER TABLE `mobilenumbers`
  ADD PRIMARY KEY (`addressID`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`trans_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `donation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mobilenumbers`
--
ALTER TABLE `mobilenumbers`
  MODIFY `addressID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
