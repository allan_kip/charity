<?php 
include './layout/header.php';
include './lib/functions.php';

$conn = connect();

$eid=$_GET['post'];


	$query = mysqli_query($conn, "SELECT * FROM events WHERE event_id= $eid") or die(mysqli_error());
	$fetch = mysqli_fetch_array($query);
	$image=$fetch['event_image'];
	$status=$fetch['status'];
	if($status<2){
		$stat="<span class='badge badge-light-success me-auto'>Approved</span>";
		
	}else{
		$stat="<span class='badge badge-light-warning me-auto'>Draft</span>";
	}
    $date = $fetch['created_on'];
    $re=date_create($date);




?>

<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" data-bs-offset="200"
    class="bg-white position-relative">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Header Section-->
        <div class="m-0">
            <!-- Custom header -->
            <div class="landing-header2 m-0 bg-light " >
            <!--begin::Container-->
            <div class="d-flex flex-row mx-auto align-items-center  ">
                <div class="m-2 "><span class=" text-dark  "><h3><i class="fas fa-2x fa-phone-alt"></i> &nbsp; Call Now: 0722831555</h5></span></div>
                <div class="m-2"><span class=" text-dark  "><h3><i class="fas fa-2x fa-envelope"></i> &nbsp; Email us: info@duarayangu.org</h3></span></div>
            </div>
            <!--end::Container-->
            </div>
            <!--begin::Header-->
            <div class="landing-header mb-13" data-kt-sticky="true" data-kt-sticky-name="landing-header"
                data-kt-sticky-offset="{default: '200px', lg: '300px'}">
                <!--begin::Container-->
                <div class="container-xxl">
                    <!--begin::Wrapper-->
                    <div class="d-flex align-items-center">
                        <!--begin::Logo-->
                        <div class="d-flex align-items-center me-8">
                            <!--begin::Mobile menu toggle-->
                            <button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none ms-n2"
                                id="kt_landing_menu_toggle">
                                <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <path
                                            d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
                                            fill="black" />
                                        <path opacity="0.3"
                                            d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                            <!--end::Mobile menu toggle-->
                            <!--begin::Logo image-->
                            <a href="./index.php">
                                <img alt="Logo" src="./images/dylogo.png" class="h-70px mt-1 pt-0" />
                            </a>
                            <!--end::Logo image-->
                        </div>
                        <!--end::Logo-->
                        <!--begin::Menu wrapper-->
                        <div class="d-lg-block" id="kt_header_nav_wrapper">
                            <div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu"
                                data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                                data-kt-drawer-width="200px" data-kt-drawer-direction="start"
                                data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true"
                                data-kt-swapper-mode="prepend"
                                data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
                                <!--begin::Menu-->
                                <div class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-dark menu-state-title-primary nav nav-flush fs-3 fw-bold"
                                    id="kt_landing_menu">
                                    



                                        <!--end::Trigger-->
                                        <div class="menu-item">
                                            <!--begin::Menu link-->
                                            <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="index.php"
                                                data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Home</a>
                                            <!--end::Menu link-->
                                        </div>
                                        <div class="menu-item">
                                            <!--begin::Menu link-->
                                            <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="index.php#work"
                                                data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Events</a>
                                            <!--end::Menu link-->
                                        </div>
                                        <!--end::Menu-->
                                        <!--begin::Menu item-->

                                        <!--begin::Menu item-->



                                
                                  
                                </div>
                                <!--end::Menu-->
                            </div>
                        </div>
                        <!--end::Menu wrapper-->
                        <!--begin::Toolbar-->
                        <div class="flex-equal text-end ms-1 position-relative z-index-2">
                            <a href="index.php#donate" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true" class="btn btn-danger">Donate</a>
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
        </div>
        <!--end::Header Section-->

        <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Post card-->
            <div class="card">
                <!--begin::Body-->
                <div class="card-body p-lg-10 pb-lg-0">
                    <!--begin::Layout-->
                    <div class="d-flex flex-column flex-xl-row">
                        <!--begin::Content-->
                        <div class="flex-lg-row-fluid me-xl-15">
                            <!--begin::Post content-->
                            <div class="mb-17">
                                <!--begin::Wrapper-->
                                <div class="mb-8">
                                    <!--begin::Container-->
                                    <div class="overlay mt-0">
                                        <!--begin::Image-->
                                        <div class="bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-325px" style="background-image:url('./images/<?php echo $fetch['event_image']?>')"></div>
                                        <!--end::Image-->
                                        <!--begin::Links-->
                                        
                                        <!--end::Links-->
                                    </div>
                                    <!--end::Container-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Body-->
                                <div class="p-0">
                                    <!--begin::Info-->
                                    <div class="d-flex align-items-center justify-content-between pb-4">
                                        <!--begin::Date-->
                                        <div class="text-gray-500 fs-5">
                                            <!--begin::Date-->
                                            <span class="me-2 fw-bolder">Posted <?php echo date_format($re,"l F d, Y") ?>.  </span>
                                            <!--end::Date-->
                                            <!--begin::Author-->
                                            <!-- <span class="fw-bold">Budget: // echo $fetch['budget'] ?></span> -->
                                            <!--end::Author-->
                                        </div>
                                        <!--end::Date-->
                                        <!--begin::Action-->
                                        <!-- <span class="text-gray-500 me-2 fw-bolder fs-5"> //echo $stat ?></span> -->
                                        <!--end::Action-->
                                    </div>
                                    <!--end::Info-->
                                    <!--begin::Title-->
                                    <a href="#" class="fw-bolder text-dark mb-3 fs-2hx lh-sm text-hover-primary"><?php echo $fetch['event_title']?></a>
                                    <!--end::Title-->
                                    <!--begin::Text-->
                                    <div class="fs-5 fw-bold text-gray-600 mt-4">
                                        <!--begin::Text-->
                                        <p class="mb-8">
                                            <?php echo $fetch['event_info']?>.
                                        </p>
                                        <!--end::Text-->
                                        
                                    </div>
                                    <!--end::Text-->
                                    <!--end::Body-->
                                </div>
                                <!--end::Post content-->
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Layout-->
                        <!--begin::Sidebar-->
                        <div class="flex-column flex-lg-row-auto w-100 w-xl-300px mb-10">
                            
                            <!--begin::Recent posts-->
                            <div class="card card-flush bg-transparent mb-9 card-p-0 border-0 shadow-0">
                                <!--begin::Card header-->
                                <div class="card-header">
                                    <h4 class="card-title fw-bolder">Recent events</h4>
                                </div>
                                <!--end::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body pt-2">
                                    <?php
                                     $n=0;
                                     $n++;
                                     $que = mysqli_query($conn, "SELECT * FROM events") or die(mysqli_error());
                                     while($row = mysqli_fetch_array($que)){
                                    ?>
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-center mb-7">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60px symbol-2by3 me-4">
                                            <div class="symbol-label" style="background-image: url('./images/<?php echo $row['event_image']?>')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="m-0">
                                            <a href="event.php?post=<?php echo $row['event_id']?>" class="text-dark fw-bolder text-hover-primary fs-6"><?php echo $row['event_title']?></a>
                                            <!-- <span class="text-gray-600 fw-bold d-block pt-1 fs-8">We’ve been a focused on making a the sky</span> -->
                                        </div>
                                        <!--end::Title-->
                                    </div>
                                    <!--end::Item-->
                                    <?php
                                     }
                                     ?>
                                </div>
                                <!--end::Card body-->
                            </div>
                            <!--end::Recent posts-->
                        </div>
                        <!--end::Sidebar-->
                    </div>
                
                    <!--end::Body-->
                </div>
                <!--end::Post card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Content-->
    </div>
       

        

        <!-- Toast for the page when become a mentor is successful -->

        

<!--begin::Button-->
<!-- <button type="button" class="btn btn-primary" id="kt_docs_toast_stack_button">Toggle Toast</button> -->
<!--end::Button-->

<!--begin::Success Toast for become and School-->
<div id="mentor_success" class="toast-container position-fixed top-1 end-0 p-3 z-index-3">
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-kt-docs-toast="stack">
        <div class="toast-header">
            <i class="far fa-lightbulb text-success fs-3"></i>
            <strong class="me-auto">&nbsp;&nbsp;Duara yangu Notification</strong>
            <small>Just now</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Application successful. Be on the lookout for our Email.
            Thank you!
        </div>
    </div>
</div>
<!--end::Toast-->

<!--begin::Failed Toast for become-->
<div id="toast_failed" class="toast-container position-fixed top-1 end-0 p-3 z-index-3">
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-kt-docs-toast="stack">
        <div class="toast-header">
            <i class="far fa-times fs-3"></i>
            <!-- <i class="far fa-lightbulb text-success fs-3"></i> -->
            <strong class="me-auto">&nbsp;&nbsp;Duara yangu Notification</strong>
            <small>Just now</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Application Failed. Try again or contact us! 
            :(
        </div>
    </div>
</div>
<!--end::Toast-->



<?php
include './layout/footer.php';
?>