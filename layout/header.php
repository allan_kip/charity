<!DOCTYPE html>

<html lang="en">


<head>
    <title>Duara Yangu Foudation </title>
    <meta charset="utf-8" />
    <meta name="description"
        content="Duara Yangu's purpose is to encourage girls to make decisions that will help them and their communities escape poverty.We envision a society devoid of poverty, 
        where everyone can find the aid they need promptly and simply, and service providers are held accountable. " />
    <meta name="keywords"
        content="Duara, Yangu, Donation,Foundation,girls,send money, donate, How to empower, How to donate, mchango" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <!-- cleaning of the meta tags -->
    <meta property="og:title" content="Duara Yangu Foundation" />
    <meta property="og:url"
        content="https://duarayangu.org" />
    <meta property="og:site_name" content="duarayangu" />
    <link rel="canonical" href="#" />
    <link rel="shortcut icon" href="./images/favicon.png" />
    <link href="./assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <link href="./assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="./assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>