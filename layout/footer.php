        <!--begin::Footer Section-->
        <div class="mb-0">
            <!--begin::Wrapper-->
            <div class="landing-dark-bg ">
                <!--begin::Container-->
                <div class="container">
                    <!--begin::Row-->
                    <div class="row py-10 py-lg-20">
                        <!--begin::Col-->
                        <div class="col-lg-6 mb-10 mb-lg-0 mx-xs-5">
                            <!--begin::Block-->
                            <div class="rounded landing-border p-9 mb-6 ">
                                <!--begin::Title-->
                                <h2 class="text-gray-900 fw-bold mb-3 ">
                                    Would you like to hear from us?
                                </h2>
                                <!--end::Title-->
                                <!--begin::Text-->
                                <span class="fw-normal fs-4 text-gray-600">Email us on
                                    <a href="#"
                                        class="fw-bold fs-4 text-gray-800 text-hover-primary">info@duarayangu.org</a></span>
                                <!--end::Text-->
                            </div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="rounded landing-border p-9 ">
                                <!--begin::Title-->
                                <h2 class="text-gray-900 fw-bold mb-3">
                                    How about you give us a call?
                                </h2>
                                <!--end::Title-->
                                <!--begin::Text-->
                                <span class="fw-normal fs-4 text-gray-700">Reach us on:
                                    <a href="#"
                                        class="fw-bold fs-4 text-gray-800 text-hover-primary">Phone:
                                        0722831555</a></span>
                                <!--end::Text-->
                            </div>
                            <!--end::Block-->
                        </div>
                        <!--end::Col-->
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <!--begin::Navs-->
                            <div class="d-flex justify-content-center">
                                <!--begin::Links-->
                                <!--More about Duara-->
                                <!--<div class="d-flex fw-bold flex-column me-20">-->
                                    <!--begin::Subtitle-->
                                <!--    <h4 class="fw-bolder text-gray-900 mb-6">-->
                                <!--        More from Duara Yangu Foundation</h4>-->
                                    <!--end::Subtitle-->
                                    <!--begin::Link-->
                                <!--    <a href="#" class="text-gray-700 text-hover-primary fs-5 mb-6">FAQ</a>-->
                                    <!--end::Link-->
                                    <!--begin::Link-->
                                    <!-- <a href="#" class="text-gray-700 text-hover-primary fs-5 mb-6">Blogs</a> -->
                                    <!--end::Link-->
                                    <!--begin::Link-->
                                <!--    <a href="./Donate.html" class="text-gray-700 text-hover-primary fs-5 mb-6">Donate</a>-->
                                    <!--end::Link-->
                                
                                <!--</div>-->
                                <!--end::Links-->
                                <!--begin::Links-->
                                <div class="d-flex fw-bold flex-column ms-lg-20">
                                    <!--begin::Subtitle-->
                                    <h4 class="fw-bolder text-gray-900 mb-6">Stay Connected</h4>
                                    <!--end::Subtitle-->
                                    <!--begin::Link-->
                                    <!--<a href="#" class="mb-6">-->
                                    <!--    <img src="./assets/media/svg/brand-logos/facebook-4.svg" class="h-20px me-2"-->
                                    <!--        alt="" />-->
                                    <!--    <span class="text-gray-700 text-hover-primary fs-5 mb-6">Facebook</span>-->
                                    <!--</a>-->
                                    <!--end::Link-->
                                   
                                    <!--begin::Link-->
                                    <!--<a href="#" class="mb-6">-->
                                    <!--    <img src="./assets/media/svg/brand-logos/twitter.svg" class="h-20px me-2"-->
                                    <!--        alt="" />-->
                                    <!--    <span class="text-gray-700 text-hover-primary fs-5 mb-6">Twitter</span>-->
                                    <!--</a>-->
                                    <!--end::Link-->
                                    
                                    <!--begin::Link-->
                                    <a href="https://www.instagram.com/duarayanguafrica/?hl=en" class="mb-6">
                                        <img src="./assets/media/svg/brand-logos/instagram-2016.svg" class="h-20px me-2"
                                            alt="" />
                                        <span class="text-gray-700 text-hover-primary fs-5 mb-6">Instagram</span>
                                    </a>
                                    <!--end::Link-->
                                </div>
                                <!--end::Links-->
                            </div>
                            <!--end::Navs-->
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Container-->
                <!--begin::Separator-->
                <div class="separator separator-solid"></div>
                <!--end::Separator-->
                <!--begin::Container-->
                <div class="container">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column flex-md-row flex-stack py-8">
                        <!--begin::Copyright-->
                        <div class="d-flex align-items-center order-2 order-md-1">
                            <!--begin::Logo-->
                            <a href="#" class="mt-0 pt-0">
                                <img alt="Logo" src="./images/dylogo.png" class="h-50px h-md-50px" />
                            </a>
                            <!--end::Logo image-->
                            <!--begin::Logo image-->
                            <span class="mx-5 fs-6 fw-bold text-gray-700 pt-1" href="#"> © Duara Yangu Foundation
                                 <script> 
                                 let todaydate = new Date ();
                                 let tdate = todaydate.getFullYear();
                                 document.write(tdate); 
                                 </script>
                              | Designed by
                            IndigoRegime </span>
                            <!--end::Logo image-->
                        </div>
                        <!--end::Copyright-->
                        <!--begin::Menu-->
                        <ul class="menu menu-gray-700 menu-hover-primary fw-bold fs-5 fs-md-5 order-1 mb-5 mb-md-0">
                            <li class="menu-item">
                                <a href="index.php#home" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true" 
                                target="_blank" class="menu-link px-2">About</a>
                            </li>
                            <li class="menu-item">
                                <a href="index.php#donate" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true"
                                    target="_blank" class="menu-link px-2">Donate</a>
                            </li>
                        </ul>
                        <!--end::Menu-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Footer Section-->
        <!--begin::Scrolltop-->
        <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
            <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
            <span class="svg-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                        fill="black" />
                    <path
                        d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                        fill="black" />
                </svg>
            </span>
            <!--end::Svg Icon-->
        </div>
        <!--end::Scrolltop-->
    </div>
    <!--end::Root-->
    <!--end::Main-->
    <!--begin::Javascript-->
    <script>
        var hostUrl = "./assets/index.php";
    </script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="./assets/plugins/global/plugins.bundle.js"></script>
    <script src="./assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="./assets/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="./assets/js/custom/pages/pricing/general.js"></script>
		<script src="./assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="./assets/js/widgets.bundle.js"></script>
		<script src="./assets/js/custom/widgets.js"></script>
		<script src="./assets/js/custom/apps/chat/chat.js"></script>
		<script src="./assets/js/custom/intro.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/type.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/budget.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/settings.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/team.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/files.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/complete.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-project/main.js"></script>
		<script src="./assets/js/custom/utilities/modals/upgrade-plan.js"></script>
		<script src="./assets/js/custom/utilities/modals/create-app.js"></script>
		<script src="./assets/js/custom/utilities/modals/new-target.js"></script>
        <script src="./assets/js/custom/utilities/modals/users-search.js"></script>
        <script src="./assets/plugins/custom/formrepeater/formrepeater.bundle.js"></script>

<script>
//donate form validation in js


// Define form element
const form = document.getElementById('donate_mentors');

// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
var validator = FormValidation.formValidation(
    form,
    {
        fields: {
           
            'cardamount': {
                validators: {
                    //if statement
                    // if(this.fields
                    notEmpty: { 
                        message: 'Select an Amount' 
                    }
                }
            },
            'payment_method': {
                validators: {
                    notEmpty: {
                        message: 'Payment method is required'
                    }
                }
            },
         },
      

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    }
);

// Submit button handler
const submitButton = document.getElementById('mentor_submit');

submitButton.addEventListener('click', function (e) {
    // Prevent default button action
    e.preventDefault();

    // Validate form before submit
    if (validator) {
        validator.validate().then(function (status) {
            console.log('validated!');

            if (status == 'Valid') {
                // Show loading indication
                submitButton.setAttribute('data-kt-indicator', 'on');

                // Disable button to avoid multiple click
                submitButton.disabled = true;

                // Simulate form submission. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                setTimeout(function () {
                    // Remove loading indication
                    submitButton.removeAttribute('data-kt-indicator');

                    // Enable button
                    submitButton.disabled = false;

                    // Show popup confirmation
                    Swal.fire({
                        text: "Form has been successfully submitted!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    form.submit(); // Submit form
                }, 2000);
            }
        });
    }
});


</script>
<script>
//donate form validation in js


// Define form element
const form = document.getElementById('support_girl');

// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
var validator = FormValidation.formValidation(
    form,
    {
        fields: {
           
            'cardamount': {
                validators: {
                    //if statement
                    // if(this.fields
                    notEmpty: { 
                        message: 'Select an Amount' 
                    }
                }
            },
            'payment_method': {
                validators: {
                    notEmpty: {
                        message: 'Payment method is required'
                    }
                }
            },
         },
      

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    }
);

// Submit button handler
const submitButton = document.getElementById('support_girl2');

submitButton.addEventListener('click', function (e) {
    // Prevent default button action
    e.preventDefault();

    // Validate form before submit
    if (validator) {
        validator.validate().then(function (status) {
            console.log('validated!');

            if (status == 'Valid') {
                // Show loading indication
                submitButton.setAttribute('data-kt-indicator', 'on');

                // Disable button to avoid multiple click
                submitButton.disabled = true;

                // Simulate form submission. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                setTimeout(function () {
                    // Remove loading indication
                    submitButton.removeAttribute('data-kt-indicator');

                    // Enable button
                    submitButton.disabled = false;

                    // Show popup confirmation
                    Swal.fire({
                        text: "Form has been successfully submitted!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    form.submit(); // Submit form
                }, 2000);
            }
        });
    }
});


</script>
<script>
//donate form validation in js


// Define form element
const form = document.getElementById('donate_mentors');

// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
var validator = FormValidation.formValidation(
    form,
    {
        fields: {
           
            'cardamount': {
                validators: {
                    //if statement
                    // if(this.fields
                    notEmpty: { 
                        message: 'Select an Amount' 
                    }
                }
            },
            'payment_method': {
                validators: {
                    notEmpty: {
                        message: 'Payment method is required'
                    }
                }
            },
         },
      

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    }
);

// Submit button handler
const submitButton = document.getElementById('mentor_submit');

submitButton.addEventListener('click', function (e) {
    // Prevent default button action
    e.preventDefault();

    // Validate form before submit
    if (validator) {
        validator.validate().then(function (status) {
            console.log('validated!');

            if (status == 'Valid') {
                // Show loading indication
                submitButton.setAttribute('data-kt-indicator', 'on');

                // Disable button to avoid multiple click
                submitButton.disabled = true;

                // Simulate form submission. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                setTimeout(function () {
                    // Remove loading indication
                    submitButton.removeAttribute('data-kt-indicator');

                    // Enable button
                    submitButton.disabled = false;

                    // Show popup confirmation
                    Swal.fire({
                        text: "Form has been successfully submitted!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    form.submit(); // Submit form
                }, 2000);
            }
        });
    }
});


</script>
        <script >
        "use strict";
        var KTFormRepeaterBasic = {
        init: function () {
            $("#kt_docs_repeater_basic").repeater({
            initEmpty: !1,
            defaultValues: { "text-input": "foo" },
            show: function () {
                $(this).slideDown();
            },
            hide: function (t) {
                $(this).slideUp(t);
            },
            });
        },
        };
        KTUtil.onDOMContentLoaded(function () {
        KTFormRepeaterBasic.init();
        });
        //
        

        var myDropzone = 
        new Dropzone("#kt_dropzonejs_example_1", {
            url: "#", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: ".pdf,.doc,.docx",
            accept: function(file, done) {
                let exten =  file.name;
                let exten2 = exten.split(/[.]/); 
                let extensionlen = exten2.length - 1;
                let extension = exten2[extensionlen];
                console.log(extension);
                done();
                // if (extension == "docx"|| extension == "pdf" || extension == "doc") {
                //     doneFile();
                // } else {
                //     done("Naha, you don't");
                // }
            },
            sending: function (){}
        });


        </script>
        <script>   
         //Toast function 

// Select elements
const button = document.getElementById('kt_docs_toast_stack_button');
const container = document.getElementById('mentor_success');
const targetElement = document.querySelector('[data-kt-docs-toast="stack"]'); // Use CSS class or HTML attr to avoid duplicating ids

// Remove base element markup
targetElement.parentNode.removeChild(targetElement);
let page_url =  window.location.href;
let sliced_url = page_url.split("=");

if(sliced_url[1] == "Successfull%20operation." || sliced_url[1] == "Records%20inserted%20successfully."){
    console.log(sliced_url[1]);
     // Create new toast element
     const newToast = targetElement.cloneNode(true);
    container.append(newToast);

    // Create new toast instance --- more info: https://getbootstrap.com/docs/5.1/components/toasts/#getorcreateinstance
    const toast = bootstrap.Toast.getOrCreateInstance(newToast);

    // Toggle toast to show --- more info: https://getbootstrap.com/docs/5.1/components/toasts/#show
    toast.show();
}else{
    let sliced_url_io = page_url.split("error=");
    
    if(sliced_url_io[1]){
        //whole toast function
const button = document.getElementById('kt_docs_toast_stack_button');
const container = document.getElementById('toast_failed');
const targetElement = document.querySelector('[data-kt-docs-toast="stack"]'); // Use CSS class or HTML attr to avoid duplicating ids

// Remove base element markup
targetElement.parentNode.removeChild(targetElement);
const newToast = targetElement.cloneNode(true);
    container.append(newToast);

    // Create new toast instance --- more info: https://getbootstrap.com/docs/5.1/components/toasts/#getorcreateinstance
    const toast = bootstrap.Toast.getOrCreateInstance(newToast);

    // Toggle toast to show --- more info: https://getbootstrap.com/docs/5.1/components/toasts/#show
    toast.show();

    }
   
}
// // Handle button click
// button.addEventListener('click', e => {
//     e.preventDefault();

//     // Create new toast element
//     const newToast = targetElement.cloneNode(true);
//     container.append(newToast);

//     // Create new toast instance --- more info: https://getbootstrap.com/docs/5.1/components/toasts/#getorcreateinstance
//     const toast = bootstrap.Toast.getOrCreateInstance(newToast);

//     // Toggle toast to show --- more info: https://getbootstrap.com/docs/5.1/components/toasts/#show
//     toast.show();
// });


            </script>
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->
</body>
<!--end::Body-->
</html>