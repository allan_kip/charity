<?php
// PHP-MAILER Configurations
define('__ROOT__', dirname(dirname(__FILE__))); 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require_once (__ROOT__ .'/vendor/phpmailer/phpmailer/src/Exception.php');
require_once  (__ROOT__ .'/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once  (__ROOT__ .'/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once  (__ROOT__ .'/vendor/autoload.php');
//load env variables

$dotenv = Dotenv\Dotenv::createImmutable(realpath('.'));
$dotenv->load();

// Email function that will be throughout 
function send_single_mail($receiver_name, $receiver_mail, $subject, $email_template){
  
$mail = new PHPMailer(true);
$email_message =  file_get_contents($email_template);
$email_message = str_replace('${user-name}', $receiver_name, $email_message); 

try {
    //Server settings
    $mail->SMTPDebug = 0;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = $_ENV['SENDERS_EMAIL_HOST'];                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $_ENV['SENDERS_EMAIL_ADDRESS'];                     //SMTP username
    $mail->Password   = $_ENV['SENDERS_EMAIL_PASSWORD'];                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = $_ENV['SENDERS_EMAIL_PORT'];                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom($_ENV['SENDERS_EMAIL_ADDRESS'], $subject);
    $mail->addAddress($receiver_mail, $receiver_name);     //Add a recipient

    //Content
    $mail->MsgHTML($email_message);
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = $subject;

    $mail->send();
    // echo 'Message has been sent';
    // return true;
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    // return false;
}
    //get email-template contents and replace.

}


 ?>