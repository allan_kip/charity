<?php
include 'common_func.php';

function connect(){
    $hostname = $_ENV['DB_HOSTNAME'];
    $username = $_ENV['DB_USERNAME'];
    $password = $_ENV['DB_PASSWORD'];
    $db = $_ENV['DB_NAME'];
    //Mysqli 
    $link = mysqli_connect($hostname, $username, $password,$db);
    if (!$link) {
        die("Connection failed: " . mysqli_connect_error());
    }else{
        return $link;
    }
}

function file_processor($fileData){
    $fileName= md5($fileData['name']);
    $fileType= $fileData['type'];
    $fileTempname= $fileData['tmp_name'];
    $fileError=    $fileData['error'];
    $fileSize= $fileData['size'];
    $fileFiledata = explode('/',$fileType);
    $size_of_array= sizeof($fileFiledata);
    $trial2 = explode(".",$fileData["name"]);
    $fileFileextension = $trial2[1];
    if($fileFileextension == "docx" ||$fileFileextension == "pdf" || $fileFileextension == "doc" || $fileFileextension == "odt"    ){
        $destination="assets/documents/";
        $fileNewName= $destination.$fileName; //hashing of the location
        if(move_uploaded_file($fileTempname,$fileNewName)){
            $fileData = $fileNewName;
            return  $fileData;
        }
    }else{
        $error =  'Wrong File type';
        return $error;
    }

}
function mentor_insert($fname,$lname,$location,$email,$mobileitems,$fileitem,$info,$date,$type){
    $conn = connect();
    $document =  file_processor($fileitem);
    
    // if($document == 'Wrong File type'){
    //     header('Location:index.php?wrong file type');
    // }else{
        //check email to prevent duplicate entry
        $emailsql = "SELECT member_email FROM `member` WHERE member_email ='$email'";
        $run_query = mysqli_query($conn,$emailsql);
        $count = mysqli_num_rows($run_query);
        if($count>0){
        $status = "Job Application already exist";
        header("Location:index.php?error=".$status);
        }else{
            //new entry
            $membername = $fname.$lname;
            $sql =  "INSERT INTO member(member_name,member_email,member_address,member_doc,member_info,created_on,member_type) VALUES (?,?,?,?,?,?,?)";
            if($stmt = mysqli_prepare($conn, $sql)){
                mysqli_stmt_bind_param($stmt, "sssssss", $membername, $email, $location, $document, $info, $date, $type);
                if(mysqli_stmt_execute($stmt)){
                        $last_id = mysqli_insert_id($conn);
                        // send mail
                        $membname = $fname.' '.$lname;
                        $emailtemplate = __ROOT__.'/email_templates/document_notification.php';
                        $subject = "Become a mentor application.";
                     
                        send_single_mail($membname,$email,$subject,$emailtemplate);
                        //insert mobile numbers 
                        foreach($mobileitems as $mobilenumber){
                            $mobile = $mobilenumber['mobilephone'];
                            $fetchsql = "INSERT INTO mobilenumbers(memberID,phone_number) VALUES($last_id, $mobile)";
                            if(mysqli_query($conn, $fetchsql)){
                           
                                $status = "Successfull operation.";
                                header("Location:index.php?message=".$status);
                              
                            } else{
                            // $status =  "ERROR: Could not able to execute $fetchsql. " . mysqli_error($conn);
                                //error code 04 is an execution error
                                header("Location:index.php?error=code04");
                            }
                        }
                        
                        
                    }else{
                    //echo "ERROR: Could not execute query: $sql. " . mysqli_error($conn);
                    //error code 04 is an execution error
                    header("Location:index.php?error=code04");
                    }
            }else{
                // echo "ERROR: Could not prepare query: $sql. " . mysqli_error($conn);
                    //error code 05 is a preparation error
                    // die(error001);

                    header("Location:index.php?errorcode05");
            }
        }
    // }
}
function school_insert($scname,$sclocation,$scemail,$scmobile,$scpname,$scpmobile,$date,$type){
$conn = connect();
$sql = "INSERT INTO member(member_name,member_address,member_email,member_tel,school_principal,principal_tel,created_on,member_type) VALUES(?,?,?,?,?,?,?,?) ";
    if($stmt = mysqli_prepare($conn,$sql)){
        mysqli_stmt_bind_param($stmt , "ssssssss",$scname,$sclocation,$scemail,$scmobile,$scpname,$scpmobile,$date, $type);
        if(mysqli_stmt_execute($stmt)){
            $emailtemplate_2 = __ROOT__.'/email_templates/school_notification_email.php';
            $subject = "Bring Duara to your school.";
         
            send_single_mail($scname,$scemail,$subject,$emailtemplate_2);
            $status = "Records inserted successfully.";  
            header("Location:index.php?status=".$status);
    }else {
        $status = "Records not inserted successfully.";  
        header("Location:index.php?error=".$status);
    }
}
}
