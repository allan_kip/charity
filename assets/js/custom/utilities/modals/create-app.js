"use strict";
var KTCreateApp = (function () {
  var e,
    t,
    o,
    r,
    a,
    i,
    n = [];
    // let form = document.getElementById
  return {
    init: function () {
      (e = document.querySelector("#kt_modal_create_app")) &&
        (new bootstrap.Modal(e),
        (t = document.querySelector("#kt_modal_create_app_stepper")),
        (o = document.querySelector("#kt_modal_create_app_form")),
        (r = t.querySelector('[data-kt-stepper-action="submit"]')),
        (a = t.querySelector('[data-kt-stepper-action="next"]')),
        (i = new KTStepper(t)).on("kt.stepper.changed", function (e) {
          3 === i.getCurrentStepIndex()
            ? (r.classList.remove("d-none"),
              r.classList.add("d-inline-block"),
              a.classList.add("d-none"))
            : 4 === i.getCurrentStepIndex()
            ? (r.classList.add("d-none"), a.classList.add("d-none"))
            : (r.classList.remove("d-inline-block"),
              r.classList.remove("d-none"),
              a.classList.remove("d-none"));
        }),
        i.on("kt.stepper.next", function (e) {
          console.log("stepper.next");
          var t = n[e.getCurrentStepIndex() - 1];
          t
            ? t.validate().then(function (t) {
                console.log("validated!"),
                  "Valid" == t
                    ? e.goNext()
                    : Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: { confirmButton: "btn btn-light" },
                      }).then(function () {});
              })
            : (e.goNext(), KTUtil.scrollTop());
        }),
        i.on("kt.stepper.previous", function (e) {
          console.log("stepper.previous"), e.goPrevious(), KTUtil.scrollTop();
        }),
        r.addEventListener("click", function (e) {
          n[2].validate().then(function (t) {
            console.log("validated! fom"),
              "Valid" == t 
                ? (e.preventDefault(),
                  (r.disabled = !0),
                  r.setAttribute("data-kt-indicator", "on"),
                  setTimeout(function () {
                    r.removeAttribute("data-kt-indicator"),
                      (r.disabled = !1),
                      i.goNext();
                  }, 2e3),
                  o.submit())
                : Swal.fire({
                    text: "Sorry, looks like there are some errors detected, please try again.",
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: { confirmButton: "btn btn-light" },
                  }).then(function () {
                    KTUtil.scrollTop();
                  });
          });
        }),
     
        n.push(
          FormValidation.formValidation(o, {
            fields: {
              fname: {
                validators: { notEmpty: { message: "First name is required" } },
              },
              lname: {
                validators: { notEmpty: { message: "Last name is required" } },
              },
              mobilephone: {
                validators: {
                  notEmpty: { message: "Mobile number is required" },
                  digits: { message: "Mobile number must contain only digits" },
                  stringLength: {
                    min: 10,
                    max: 12,
                    message: "Mobile number can contain either 10 or 12 digits.eg(07xx),(2547xx)",
                  },
                },
              },
              location: {
                validators: { notEmpty: { message: "Location is required" } },
              },
              email: {
                validators: { 
                  notEmpty: { message: "Email is required" },
                  emailAddress: { message: "The value is not a valid email address" }
               },
              },
              info: {
                validators: { notEmpty: { message: "Brief information about you is required" } },
                // stringLength: {
                //   min: 10,
                //   max: 12,
                //   message: "Mobile number can contain either 10 or 12 digits.eg(07xx),(2547xx)",
                // },
              },
           
            },
            plugins: {
              trigger: new FormValidation.plugins.Trigger(),
              bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
              }),
            },
          })
        ),
        n.push(
          FormValidation.formValidation(o, {
            fields: {
              fileToUpload: {
                validators: { notEmpty: { message: "Please select your CV" } ,
                file: {
                  extension: 'pdf,docx,doc',
                  message: 'The selected file is not valid'
              },},
              },
            },
            plugins: {
              trigger: new FormValidation.plugins.Trigger(),
              bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
              }),
            },
          })
        ));

    },
  };
})();
KTUtil.onDOMContentLoaded(function () {
  KTCreateApp.init();
});
