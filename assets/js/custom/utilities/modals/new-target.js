"use strict";
var KTModalNewTarget = (function () {
  var t, e, n, a, o, i;
  return {
    init: function () {
      (i = document.querySelector("#kt_modal_new_target")) &&
        ((o = new bootstrap.Modal(i)),
        (a = document.querySelector("#kt_modal_new_target_form")),
        (t = document.getElementById("kt_modal_new_target_submit")),
        (e = document.getElementById("kt_modal_new_target_cancel")),
        // new Tagify(a.querySelector('[name="tags"]'), {
        //   whitelist: ["Important", "Urgent", "High", "Medium", "Low"],
        //   maxTags: 5,
        //   dropdown: { maxItems: 10, enabled: 0, closeOnSelect: !1 },
        // }).on("change", function () {
        //   n.revalidateField("tags");
        // }),
        // $(a.querySelector('[name="due_date"]')).flatpickr({
        //   enableTime: !0,
        //   dateFormat: "d, M Y, H:i",
        // }),
        // $(a.querySelector('[name="team_assign"]')).on("change", function () {
        //   n.revalidateField("team_assign");
        // }),
        (n = FormValidation.formValidation(a, {
          fields: {
            school_name: {
              validators: { notEmpty: { message: "School name is required" } },
            },
            school_location: {
              validators: { notEmpty: { message: "School location is required" } },
            },
            school_email: {
              validators: { 
              notEmpty: { message: "School email is required" },
              emailAddress: { message: "The value is not a valid email address" } },
            },
            school_mobile: {
              validators: { 
                notEmpty: { message: "School mobile is required" },
                  digits: { message: "Mobile number must contain only digits" },
                  stringLength: {
                    min: 10,
                    max: 12,
                    message: "Mobile number can contain either 10 or 12 digits.eg(07xx),(2547xx)",
                  },
            },  
            },
            school_principal_name: {
              validators: { notEmpty: { message: "School principal's name is required" },
             },
            },
            school_principal_number: {
              validators: { 
              notEmpty: { message: "School principal's number  is required"},
              digits: { message: "Mobile number must contain only digits" },
              stringLength: {
                min: 10,
                max: 12,
                message: "Mobile number can contain either 10 or 12 digits.eg(07xx),(2547xx)",
               },
            },
            },
        
          },
          plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
              rowSelector: ".fv-row",
              eleInvalidClass: "",
              eleValidClass: "",
            }),
          },
        })),
        t.addEventListener("click", function (e) {
          e.preventDefault(),
            n &&
              n.validate().then(function (e) {
                console.log("validated!"),
                  "Valid" == e
                    ? (t.setAttribute("data-kt-indicator", "on"),
                      (t.disabled = !0),
                      setTimeout(function () {
                        t.removeAttribute("data-kt-indicator"),
                          (t.disabled = !1),
                          Swal.fire({
                            text: "Form has been successfully submitted!",
                            icon: "success",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, got it!",
                            customClass: { confirmButton: "btn btn-primary" },
                          }).then(function (t) {
                            t.isConfirmed && a.submit()&& o.hide();
                          });
                      }, 2e3))
                    : Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, got it!",
                        customClass: { confirmButton: "btn btn-primary" },
                      });
              });
        }),
        e.addEventListener("click", function (t) {
          t.preventDefault(),
            Swal.fire({
              text: "Are you sure you would like to cancel?",
              icon: "warning",
              showCancelButton: !0,
              buttonsStyling: !1,
              confirmButtonText: "Yes, cancel it!",
              cancelButtonText: "No, return",
              customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-active-light",
              },
            }).then(function (t) {
              t.value
                ? (a.reset(), o.hide())
                : "cancel" === t.dismiss &&
                  Swal.fire({
                    text: "Your form has not been cancelled!.",
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: { confirmButton: "btn btn-primary" },
                  });
            });
        }));
    },
  };
})();
KTUtil.onDOMContentLoaded(function () {
  KTModalNewTarget.init();
});
