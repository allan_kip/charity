<?php
session_start();

if (!isset($_SESSION['aid'])){
    header('location:login.php');
}
include"lib/db.php";
include"includes/header.php";
include"includes/sidebar.php";

$eid=$_GET['view'];






	$query = mysqli_query($con, "SELECT * FROM events WHERE event_id= $eid") or die(mysqli_error());
	$fetch = mysqli_fetch_array($query);
	$image=$fetch['event_image'];
	$status=$fetch['status'];
	if($status<2){
		$stat="<span class='badge badge-light-success me-auto'>Approved</span>";
		
	}else{
		$stat="<span class='badge badge-light-warning me-auto'>Draft</span>";
	}
    $date = $fetch['created_on'];
    $re=date_create($date);


?>

<!--begin::Wrapper-->
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
    <!--begin::Header-->
    <div id="kt_header" class="header align-items-stretch">
        <!--begin::Brand-->
        <div class="header-brand">
            <!--begin::Logo-->
            <a href="index.php">
                <img alt="Logo" src="../images/dylogo.png" class="h-75px" />
            </a>
            <!--end::Logo-->
        </div>
        <!--end::Brand-->
        <!--begin::Topbar-->
        <div class="topbar">
            <!--begin::Topbar container-->
            <div class="container-fluid py-6 py-lg-0 d-flex flex-column flex-sm-row align-items-lg-stretch justify-content-sm-between">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column me-5">
                    <!--begin::Title-->
                    <h1 class="d-flex flex-column text-dark fw-bolder fs-2 mb-0">Events</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="index.php" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-300 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                         <!--begin::Item-->
                         <li class="breadcrumb-item text-muted">
                            <a href="events.php" class="text-muted text-hover-primary">Events</a>
                        </li>
                        <!--end::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-300 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-dark">Event Info</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <!--begin::Action group-->
                <div class="d-flex align-items-center pt-3 pt-sm-0">
                    <!--begin::Action wrapper-->
                    <div class="header-search me-4">
                        <!--begin::Search-->
                        <div id="kt_header_search" class="d-flex align-items-center w-lg-250px" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-search-responsive="lg" data-kt-menu-trigger="auto" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end">
                            <!--begin::Tablet and mobile search toggle-->
                            <div data-kt-search-element="toggle" class="d-flex d-lg-none align-items-center">
                                <div class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                    <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Tablet and mobile search toggle-->
                            <!--begin::Form(use d-none d-lg-block classes for responsive search)-->
                            <form data-kt-search-element="form" class="d-none d-lg-block w-100 position-relative" autocomplete="off">
                                <!--begin::Hidden input(Added to disable form autocomplete)-->
                                <input type="hidden" />
                                <!--end::Hidden input-->
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                <span class="svg-icon svg-icon-3 search-icon position-absolute top-50 translate-middle-y ms-4">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black" />
                                        <path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Input-->
                                <input type="text" class="form-control bg-transparent ps-12" name="search" value="" placeholder="Search" data-kt-search-element="input" />
                                <!--end::Input-->
                                <!--begin::Spinner-->
                                <span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
                                    <span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
                                </span>
                                <!--end::Spinner-->
                                <!--begin::Reset-->
                                <span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-4" data-kt-search-element="clear">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                    <span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </span>
                                <!--end::Reset-->
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--end::Action wrapper-->
                    <!--begin::Actions-->
                    <div class="d-flex">
                        <!--begin::Notifications-->
                        <div class="d-flex align-items-center me-4">
                            <!--begin::Menu- wrapper-->
                            <a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
                                
                                <div class="symbol symbol-40px cursor-pointer" >
                                    <img src="assets/media/avatars/300-1.jpg" alt="" />
                                </div>
                            </a>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column w-250px w-lg-325px" data-kt-menu="true">
                                <!--begin::Heading-->
                                <div class="d-flex flex-column flex-center bgi-no-repeat rounded-top px-9 py-10" style="background-image:url('assets/media/misc/header-dropdown.png')">
                                    <!--begin::Status-->
                                    <span class="badge bg-primary py-2 px-3 mb-3">Hello</span>
                                    <!--end::Status-->
                                    <!--begin::Title-->
                                    <h3 class="text-white fw-bold">Admin</h3>
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                                <!--begin:Nav-->
                                <div class="row g-0">
                                    <!--begin:Item-->
                                    <div class="col-6">
                                        <a href="profile.php" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end border-bottom">
                                            <span class="fs-5 fw-bold text-gray-800 mb-0">Profile</span>
                                            <span class="fs-7 text-gray-400">View</span>
                                        </a>
                                    </div>
                                    <!--end:Item-->
                                    <!--begin:Item-->
                                    <div class="col-6">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#kt_modal_logout" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-bottom">
                                            <span class="fs-5 fw-bold text-gray-800 mb-0">Logout</span>
                                        </a>
                                    </div>
                                    <!--end:Item-->
                                    <!--begin:Item-->
                                </div>
                                <!--end:Nav-->
                            </div>
                            <!--end::Menu-->
                            <!--end::Menu wrapper-->
                        </div>
                        <!--end::Notifications-->
                        <!--begin::Theme mode-->
                        <div class="d-flex align-items-center me-4">
                            <!--begin::Menu toggle-->
                            <a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-muted btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
                                <i class="fonticon-sun fs-2"></i>
                                <i class="fonticon-moon fs-2 d-none"></i>
                            </a>
                            <!--begin::Menu toggle-->
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-muted menu-active-bg menu-state-primary fw-bold py-4 fs-6 w-200px" data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3 my-1">
                                    <a href="index-2.html" class="menu-link px-3 active">
                                        <span class="menu-icon">
                                            <i class="fonticon-sun fs-2"></i>
                                        </span>
                                        <span class="menu-title">Light</span>
                                    </a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3 my-1">
                                    <a href="dark/index.html" class="menu-link px-3">
                                        <span class="menu-icon">
                                            <i class="fonticon-moon fs-2"></i>
                                        </span>
                                        <span class="menu-title">Dark</span>
                                    </a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Theme mode-->
                    </div>
                    <!--end::Actions-->
                </div>
                <!--end::Action group-->
            </div>
            <!--end::Topbar container-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Header-->
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <?php

            if(isset($_SESSION['error'])){
                echo "
                  <div class='alert alert-danger text-center'>
                  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p>".$_SESSION['error']."</p> 
                  </div>
                ";
                unset($_SESSION['error']);
            }
        
            if(isset($_SESSION['success'])){
                echo "
                  <div class='alert alert-success text-center'>
                  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p>".$_SESSION['success']."</p> 
                  </div>
                ";
                unset($_SESSION['success']);
            }
            ?>
            <!--begin::Post card-->
            <div class="card">
                <!--begin::Body-->
                <div class="card-body p-lg-10 pb-lg-0">
                    <!--begin::Layout-->
                    <div class="d-flex flex-column flex-xl-row">
                        <!--begin::Content-->
                        <div class="flex-lg-row-fluid me-xl-15">
                            <!--begin::Post content-->
                            <div class="mb-17">
                                <!--begin::Wrapper-->
                                <div class="mb-8">
                                    <!--begin::Container-->
                                    <div class="overlay mt-0">
                                        <!--begin::Image-->
                                        <div class="bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-325px" style="background-image:url('../images/<?php echo $fetch['event_image']?>')"></div>
                                        <!--end::Image-->
                                        <!--begin::Links-->
                                        <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                                            <a href="edit.php?edit=<?php echo $fetch['event_id']?>" class="btn btn-primary">Edit</a>
                                            <!-- <a href="../careers/apply.html" class="btn btn-light-primary ms-3">Join Us</a> -->
                                        </div>
                                        <!--end::Links-->
                                    </div>
                                    <!--end::Container-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Body-->
                                <div class="p-0">
                                    <!--begin::Info-->
                                    <div class="d-flex align-items-center justify-content-between pb-4">
                                        <!--begin::Date-->
                                        <div class="text-gray-500 fs-5">
                                            <!--begin::Date-->
                                            <span class="me-2 fw-bolder">Posted <?php echo date_format($re,"l F d, Y") ?>.  </span>
                                            <!--end::Date-->
                                            <!--begin::Author-->
                                            <span class="fw-bold">Budget: <?php echo $fetch['budget'] ?></span>
                                            <!--end::Author-->
                                        </div>
                                        <!--end::Date-->
                                        <!--begin::Action-->
                                        <span class="text-gray-500 me-2 fw-bolder fs-5"><?php echo $stat ?></span>
                                        <!--end::Action-->
                                    </div>
                                    <!--end::Info-->
                                    <!--begin::Title-->
                                    <a href="#" class="fw-bolder text-dark mb-3 fs-2hx lh-sm text-hover-primary"><?php echo $fetch['event_title']?></a>
                                    <!--end::Title-->
                                    <!--begin::Text-->
                                    <div class="fs-5 fw-bold text-gray-600 mt-4">
                                        <!--begin::Text-->
                                        <p class="mb-8">
                                            <?php echo $fetch['event_info']?>.
                                        </p>
                                        <!--end::Text-->
                                        
                                    </div>
                                    <!--end::Text-->
                                    <!--end::Body-->
                                </div>
                                <!--end::Post content-->
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Layout-->
                        <!--begin::Sidebar-->
                        <div class="flex-column flex-lg-row-auto w-100 w-xl-300px mb-10">
                            
                            <!--begin::Recent posts-->
                            <div class="card card-flush bg-transparent mb-9 card-p-0 border-0 shadow-0">
                                <!--begin::Card header-->
                                <div class="card-header">
                                    <h4 class="card-title fw-bolder">Recent events</h4>
                                </div>
                                <!--end::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body pt-2">
                                    <?php
                                     $n=0;
                                     $n++;
                                     $que = mysqli_query($con, "SELECT * FROM events") or die(mysqli_error());
                                     while($row = mysqli_fetch_array($que)){
                                    ?>
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-center mb-7">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60px symbol-2by3 me-4">
                                            <div class="symbol-label" style="background-image: url('../images/<?php echo $row['event_image']?>')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="m-0">
                                            <a href="post.php?view=<?php echo $row['event_id']?>" class="text-dark fw-bolder text-hover-primary fs-6"><?php echo $row['event_title']?></a>
                                            <!-- <span class="text-gray-600 fw-bold d-block pt-1 fs-8">We’ve been a focused on making a the sky</span> -->
                                        </div>
                                        <!--end::Title-->
                                    </div>
                                    <!--end::Item-->
                                    <?php
                                     }
                                     ?>
                                </div>
                                <!--end::Card body-->
                            </div>
                            <!--end::Recent posts-->
                        </div>
                        <!--end::Sidebar-->
                    </div>
                
                    <!--end::Body-->
                </div>
                <!--end::Post card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Content-->
         <!--begin::Footer-->
        <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
            <!--begin::Container-->
            <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
                <!--begin::Copyright-->
                <div class="text-dark order-2 order-md-1">
                    <img alt="Logo" src="../images/favicon.png" class="h-80px "  />
                    <span class="text-muted fw-bold me-1">© Duara Yangu Foundation <script>document.write(new Date().getFullYear());</script> | Designed by IndigoRegime</span>
                </div>
                <!--end::Copyright-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Footer-->
    </div>
    <!--end::Wrapper-->
    </div>
    <!--end::Page-->


<?php

include"includes/footer.php";
?>