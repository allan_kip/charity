<?php
require_once realpath('../'.'vendor/autoload.php');

//load env variables
$dotenv = Dotenv\Dotenv::createImmutable(realpath('../'));
$dotenv->load();

$servername = $_ENV['DB_HOSTNAME'];
$username = $_ENV['DB_USERNAME'];
$password = $_ENV['DB_PASSWORD'];
$db = $_ENV['DB_NAME'];

// Create connection
$con = mysqli_connect($servername, $username, $password,$db);

// Check connection
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}
// else{
//     echo"Connection successful";
// }

?>