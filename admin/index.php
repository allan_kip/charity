<?php
session_start();

if (!isset($_SESSION['aid'])){
    header('location:login.php');
}
include"lib/db.php";
include"includes/header.php";
include"includes/sidebar.php";



?>

<!--begin::Wrapper-->
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header"  class="header align-items-stretch">
						<!--begin::Brand-->
						<div class="header-brand">
							<!--begin::Logo-->
							<a href="index.php">
								<img alt="Logo" src="../images/dylogo.png" class="h-75px" />
							</a>
							<!--end::Logo-->
							<!--begin::Aside toggle-->
							<div class="d-flex align-items-center d-lg-none ms-n3 me-1" title="Show aside menu">
								<div class="btn btn-icon btn-active-color-primary w-30px h-30px" id="kt_aside_mobile_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
											<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
							</div>
							<!--end::Aside toggle-->
						</div>
						<!--end::Brand-->
						<!--begin::Topbar-->
						<div class="topbar">
							<!--begin::Topbar container-->
							<div class="container-fluid py-6 py-lg-0 d-flex flex-column flex-sm-row align-items-lg-stretch justify-content-sm-between">
								<!--begin::Page title-->
								<div class="page-title d-flex flex-column me-5">
									<!--begin::Title-->
									<h1 class="d-flex flex-column text-dark fw-bolder fs-2 mb-0">Dashboard</h1>
									<!--end::Title-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="index-2.html" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Action group-->
								<div class="d-flex align-items-center pt-3 pt-sm-0">
									<!--begin::Action wrapper-->
									<div class="header-search me-4">
										<!--begin::Search-->
										<div id="kt_header_search" class="d-flex align-items-center w-lg-250px" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-search-responsive="lg" data-kt-menu-trigger="auto" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end">
											<!--begin::Tablet and mobile search toggle-->
											<div data-kt-search-element="toggle" class="d-flex d-lg-none align-items-center">
												<div class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary">
													<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
													<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
															<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</div>
											</div>
											<!--end::Tablet and mobile search toggle-->
											<!--begin::Form(use d-none d-lg-block classes for responsive search)-->
											<form data-kt-search-element="form" class="d-none d-lg-block w-100 position-relative" autocomplete="off">
												<!--begin::Hidden input(Added to disable form autocomplete)-->
												<input type="hidden" />
												<!--end::Hidden input-->
												<!--begin::Icon-->
												<!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
												<span class="svg-icon svg-icon-3 search-icon position-absolute top-50 translate-middle-y ms-4">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black" />
														<path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<!--end::Icon-->
												<!--begin::Input-->
												<input type="text" class="form-control bg-transparent ps-12" name="search" value="" placeholder="Search" data-kt-search-element="input" />
												<!--end::Input-->
												<!--begin::Spinner-->
												<span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
													<span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
												</span>
												<!--end::Spinner-->
												<!--begin::Reset-->
												<span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-4" data-kt-search-element="clear">
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
													<span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
															<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</span>
												<!--end::Reset-->
											</form>
											<!--end::Form-->
										</div>
										<!--end::Search-->
									</div>
									<!--end::Action wrapper-->
									<!--begin::Actions-->
									<div class="d-flex">
										<!--begin::Notifications-->
										<div class="d-flex align-items-center me-4">
											<!--begin::Menu- wrapper-->
											<a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												
												<div class="symbol symbol-40px cursor-pointer" >
													<img src="assets/media/avatars/300-1.jpg" alt="" />
												</div>
											</a>
											<!--begin::Menu-->
											<div class="menu menu-sub menu-sub-dropdown menu-column w-250px w-lg-325px" data-kt-menu="true">
												<!--begin::Heading-->
												<div class="d-flex flex-column flex-center bgi-no-repeat rounded-top px-9 py-10" style="background-image:url('assets/media/misc/header-dropdown.png')">
													<!--begin::Status-->
													<span class="badge bg-primary py-2 px-3 mb-3">Hello</span>
													<!--end::Status-->
													<!--begin::Title-->
													<h3 class="text-white fw-bold">Admin</h3>
													<!--end::Title-->
												</div>
												<!--end::Heading-->
												<!--begin:Nav-->
												<div class="row g-0">
													<!--begin:Item-->
													<div class="col-6">
														<a href="profile.php" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end border-bottom">
															<span class="fs-5 fw-bold text-gray-800 mb-0">Profile</span>
															<span class="fs-7 text-gray-400">View</span>
														</a>
													</div>
													<!--end:Item-->
													<!--begin:Item-->
													<div class="col-6">
														<a href="#" data-bs-toggle="modal" data-bs-target="#kt_modal_logout" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-bottom">
															<span class="fs-5 fw-bold text-gray-800 mb-0">Logout</span>
														</a>
													</div>
													<!--end:Item-->
													<!--begin:Item-->
												</div>
												<!--end:Nav-->
											</div>
											<!--end::Menu-->
											<!--end::Menu wrapper-->
										</div>
										<!--end::Notifications-->
										<!--begin::Theme mode-->
										<!--<div class="d-flex align-items-center me-4">-->
											<!--begin::Menu toggle-->
										<!--	<a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-muted btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">-->
										<!--		<i class="fonticon-sun fs-2"></i>-->
										<!--		<i class="fonticon-moon fs-2 d-none"></i>-->
										<!--	</a>-->
											<!--begin::Menu toggle-->
											<!--begin::Menu-->
										<!--	<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-muted menu-active-bg menu-state-primary fw-bold py-4 fs-6 w-200px" data-kt-menu="true">-->
												<!--begin::Menu item-->
										<!--		<div class="menu-item px-3 my-1">-->
										<!--			<a href="index-2.html" class="menu-link px-3 active">-->
										<!--				<span class="menu-icon">-->
										<!--					<i class="fonticon-sun fs-2"></i>-->
										<!--				</span>-->
										<!--				<span class="menu-title">Light</span>-->
										<!--			</a>-->
										<!--		</div>-->
												<!--end::Menu item-->
												<!--begin::Menu item-->
										<!--		<div class="menu-item px-3 my-1">-->
										<!--			<a href="dark/index.html" class="menu-link px-3">-->
										<!--				<span class="menu-icon">-->
										<!--					<i class="fonticon-moon fs-2"></i>-->
										<!--				</span>-->
										<!--				<span class="menu-title">Dark</span>-->
										<!--			</a>-->
										<!--		</div>-->
												<!--end::Menu item-->
										<!--	</div>-->
											<!--end::Menu-->
										<!--</div>-->
										<!--end::Theme mode-->
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Action group-->
							</div>
							<!--end::Topbar container-->
						</div>
						<!--end::Topbar-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluid">
							<!--begin::Row-->
							<div class="row g-5 g-xxl-10">
								<!--begin::Col-->
								<div class="col-xl-5 col-xxl-8 mb-5 mb-xxl-10">
									<!--begin::Engage widget 2-->
									<div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px h-xl-100 bg-gray-200 border-0" style="background-position: 100% 100%;background-size: 500px auto;background-image:url('assets/media/misc/img-78.jpg')">
										<!--begin::Body-->
										<div class="card-body d-flex flex-column justify-content-center ps-lg-15">
											<!--begin::Title-->
											<h3 class="text-gray-800 fs-2qx fw-boldest mb-4 mb-lg-8">Hello admin, 
											<br />Welcome Back!</h3>
											<!--end::Title-->
										</div>
										<!--end::Body-->
									</div>
									<!--end::Engage widget 2-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-xl-7 col-xxl-4 mb-xl-5 mb-xxl-10">
									<!--begin::Card widget 4-->
									<div class="card card-flush h-xl-100">
										<!--begin::Header-->
										<div class="card-header pt-5">
											<!--begin::Title-->
											<div class="card-title d-flex flex-column">
												<!--begin::Info-->
												<div class="d-flex align-items-center">
													<!--begin::Currency-->
													<span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">Ksh</span>
													<!--end::Currency-->
													<!--begin::Amount-->
													<span class="fs-2hx fw-bolder text-dark me-2 lh-1">50,820</span>
													<!--end::Amount-->
												</div>
												<!--end::Info-->
												<!--begin::Subtitle-->
												<span class="text-gray-400 pt-1 fw-bold fs-6">Total Donations</span>
												<!--end::Subtitle-->
											</div>
											<!--end::Title-->
										</div>
										<!--end::Header-->
										<!--begin::Card body-->
										<div class="card-body pt-2 pb-4 d-flex align-items-center">
											<!--begin::Chart-->
											<div class="d-flex flex-center me-5 pt-2">
												<div id="kt_card_widget_4_chart" style="min-width: 125px; min-height: 125px" data-kt-size="125" data-kt-line="18"></div>
											</div>
											<!--end::Chart-->
											<!--begin::Labels-->
											<div class="d-flex flex-column content-justify-center w-100">
												<!--begin::Label-->
												<div class="d-flex fs-6 fw-bold align-items-center my-3">
													<!--begin::Bullet-->
													<div class="bullet w-8px h-6px rounded-2 bg-primary me-3"></div>
													<!--end::Bullet-->
													<!--begin::Label-->
													<div class="text-gray-500 flex-grow-1 me-4">Donations</div>
													<!--end::Label-->
													<!--begin::Stats-->
													<div class="fw-boldest text-gray-700 text-xxl-end">Ksh 50,820</div>
													<!--end::Stats-->
												</div>
												<!--end::Label-->
												<!--begin::Label-->
												<div class="d-flex fs-6 fw-bold align-items-center">
													<!--begin::Bullet-->
													<div class="bullet w-8px h-6px rounded-2 bg-danger me-3"></div>
													<!--end::Bullet-->
													<!--begin::Label-->
													<div class="text-gray-500 flex-grow-1 me-4">Expense</div>
													<!--end::Label-->
													<!--begin::Stats-->
													<div class="fw-boldest text-gray-700 text-xxl-end">Ksh 30,000</div>
													<!--end::Stats-->
												</div>
												<!--end::Label-->
												<!--begin::Label-->
												<div class="d-flex fs-6 fw-bold align-items-center">
													<!--begin::Bullet-->
													<div class="bullet w-8px h-6px rounded-2 me-3" style="background-color: #E4E6EF"></div>
													<!--end::Bullet-->
													<!--begin::Label-->
													<div class="text-gray-500 flex-grow-1 me-4">Balance</div>
													<!--end::Label-->
													<!--begin::Stats-->
													<div class="fw-boldest text-gray-700 text-xxl-end">Ksh 20,820</div>
													<!--end::Stats-->
												</div>
												<!--end::Label-->
											</div>
											<!--end::Labels-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card widget 4-->
								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
							<!--begin::Row-->
							<div class="row g-5 g-xxl-10">
								<!--begin::Col-->
								<div class="col-xl-12 mb-5 mb-xxl-10">
									<!--begin::Table Widget 3-->
									<div class="card card-flush h-xl-100">
										<!--begin::Card header-->
										<div class="card-header py-7">
											<!--begin::Tabs-->
											<div class="card-title pt-3 mb-0 gap-4 gap-lg-10 gap-xl-15 nav nav-tabs border-bottom-0" data-kt-table-widget-3="tabs_nav">
												<!--begin::Tab item-->
												<div class="fs-4 fw-bolder pb-3 border-bottom border-3 border-primary cursor-pointer" data-kt-table-widget-3="tab" data-kt-table-widget-3-value="Show All">Donation Campaigns</div>
												<!--end::Tab item-->
											</div>
											<!--end::Tabs-->
											<!--begin::Create campaign button-->
											<div class="card-toolbar">
												<a href="donation.php" type="button" class="btn btn-danger">View</a>
											</div>
											<!--end::Create campaign button-->
										</div>
										<!--end::Card header-->
										<!--begin::Card body-->
										<div class="card-body pt-1">
											<!--begin::Seprator-->
											<div class="separator separator-dashed my-5"></div>
											<!--end::Seprator-->
											<!--begin::Table-->
											<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
                        <!--begin::Table head-->
                        <thead>
                            <!--begin::Table row-->
                            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                <th class="w-10px pe-2">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                        <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
                                    </div>
                                </th>
                                <th class="min-w-125px">Campaign Title</th>
                                <th class="min-w-125px">Target</th>
                                <th class="min-w-125px">Progress</th>
                                <th class="min-w-125px">Status</th>
                                <th class="text-end min-w-70px">Actions</th>
                                <th class="min-w-125px"></th>
                            </tr>
                            <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                            <?php
                            $n=0;
                            $n++;
                            $query = mysqli_query($con, "SELECT * FROM donation") or die(mysqli_error());
                            while($fetch = mysqli_fetch_array($query)){
                                $status=$fetch['status'];
								$did=$fetch['donation_id'];
								$target=$fetch['donation_target'];
                                if($status<2){
                                    $stat="<div class='badge badge-light-warning'>Draft</div>";
                                }else{
                                    $stat="<div class='badge badge-light-success'>Approved</div>";
                                }
								$n=0;
                                $que = mysqli_query($con, "SELECT * FROM transactions WHERE donation_campaign=$did") or die(mysqli_error());
                                while($row = mysqli_fetch_array($que)){
                                    $amount=$row['amount'];
                                    $n+=$amount;
                                }
                                $progress=$n*100/$target;
                                
                                if($progress<30){
                                    $span='bg-danger';
                                }
                                // if($progress>100){
                                //     $progress=100;
                                //     $span='bg-success';
                                // }
                                if($progress>30 && $progress<100){
                                    $span='bg-primary';
                                }

                            ?>
                            <tr>
                                <td>
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" />
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="text-gray-600 text-hover-primary mb-1"><?php echo $fetch['donation_title']?></a>
                                </td>
                                <td>Ksh <?php echo $fetch['donation_target']?></td>
                                <td>
								<div id="kt_slider_basic">
                                        <div class="progress mb-5">
                                            <div class="progress-bar <?php echo $span?>" role="progressbar" style="width: 100%;" aria-valuenow="<?php echo $progress?>"
                                                aria-valuemin="0" aria-valuemax="100"><?php echo $progress?>%</div>
                                        </div>
                                    </div>
								</td>
                                <td data-filter="published">
                                    <?php echo $stat?>
                                </td>
                                <td class="text-end">
                                    <a href="view.php?view=donation&&id=<?php echo $fetch['donation_id']?>" class="btn btn-sm btn-light btn-active-light-primary">View
                            
                                </td>
                                <td></td>
                                <!--end::Action=-->
                            </tr>

                            <?php
                            }
                            ?>
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Table Widget 3-->
								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						 <!--begin::Container-->
						 <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<img alt="Logo" src="../images/favicon.png" class="h-80px "  />
								<span class="text-muted fw-bold me-1">© Duara Yangu Foundation <script>document.write(new Date().getFullYear());</script> | Designed by IndigoRegime</span>
							</div>
							<!--end::Copyright-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->

<?php
include"includes/footer.php";
?>
