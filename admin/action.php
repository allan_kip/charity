<?php
session_start(); 
include"lib/db.php";


//login action

if (isset($_POST['login'])){
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM member WHERE member_email = '$email' AND member_pass = '$password'";
    $run_query = mysqli_query($con,$sql);
    $count = mysqli_num_rows($run_query);
    $row = mysqli_fetch_array($run_query);
    $_SESSION["aid"] = $row["member_id"];
    $_SESSION["name"] = $row["member_name"];
    $admin=$row["member_type"];

    if(empty($email) || empty($password)){
        $_SESSION['error'] = 'Fill in the form first';
        header('location:login.php');
    }
    else{
        if ($count == 1){
            if($admin < 5){
                //echo 'login_success';
                header('location: index.php');
            }
            else{
                $_SESSION['error'] = 'You are not an admin.';
                header('location:login.php');
            }
            
        }
        else{
            $_SESSION['error'] = 'Incorrect login credentials';
            header('location:login.php');
        }
    }

}


//logout

if (isset($_POST['logout'])){
    unset($_SESSION['aid']);
    unset($_SESSION['name']);

    header('location:login.php');
}


//add donation campaign
if (isset($_POST['don_campaign'])){
    $title = $_POST['title'];
    $status = $_POST['status'];
    $don_target = $_POST['target'];
    $desc =$_POST['desc'];
    $date =date('Y-m-d');
    // Get image name
    $image = $_FILES['image']['name'];

    // image file directory
    $target = "../images/".basename($image);

    $sql = "INSERT INTO donation (`donation_id`, `donation_title`,`donation_info`,`donation_target`,`created_on`,`donation_image`,`status`) 
    VALUES(NULL,'$title','$desc','$don_target','$date','$image','$status') ";
    $query = mysqli_query($con,$sql);

    if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
        $_SESSION['success'] = "Image uploaded successfully";
        header('location:donation.php');
        
    }else {
        $_SESSION['error'] = "Failed to upload image";
        header('location:donation.php');
    }
    if ($query){
        $_SESSION['success']='Donation campaign added successfully';
        header('location:donation.php');
    }
    else{
        $_SESSION['error']='Donation campaign  not added successfully';
        header('location:donation.php');
    }
}

//edit donation campaign
if (isset($_POST['edit_campaign'])){
    $id=$_POST['id'];
    $title = $_POST['title'];
    $status = $_POST['status'];
    $don_target = $_POST['target'];
    $desc =$_POST['desc'];
    // Get image name
    $image = $_FILES['image']['name'];

    // image file directory
    $target = "../images/".basename($image);

    if(!empty($image)){
        $sql = "UPDATE donation SET `donation_title`= '$title',`donation_image`='$image',`donation_info` = '$desc',`donation_target` = '$don_target',`status`='$status' WHERE donation_id = '$id' ";
        $query = mysqli_query($con,$sql);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
            
            $_SESSION['success'] = "Image uploaded successfully";
            header('location:donation.php');
            
        }else {
            $_SESSION['error'] = "Failed to upload image";
            header('location:donation.php');
        }
        if ($query){
            $_SESSION['success']='Donation campaign added successfully';
            header('location:donation.php');
        }
        else{
            $_SESSION['error']='Donation campaign  not added successfully';
            header('location:donation.php');
        }

    }
    else{

    $sql = "UPDATE donation SET `donation_title`= '$title',`donation_info` = '$desc',`donation_target` = '$don_target',`status`='$status' WHERE donation_id = '$id' ";
    $query = mysqli_query($con,$sql);

    if ($query){
        $_SESSION['success']='Donation campaign added successfully';
        header('location:donation.php');
    }
    else{
        $_SESSION['error']='Donation campaign  not added successfully';
        header('location:donation.php');
    }
    }
}

//add donation transaction
if (isset($_POST['transaction'])){
    $user = $_POST['user'];
    $status = $_POST['status'];
    $amount = $_POST['amount'];
    $campaign =$_POST['campaign'];
    $date =date('Y-m-d');
    $trans = $_POST['trans'];
    $payment=$_POST['pay'];

    $sql = "INSERT INTO transactions (`trans_id`, `transactionId`,`trans_member`,`payment_type`,`trans_date`,`donation_campaign`,`status`,`amount`) 
    VALUES(NULL,'$trans','$user','$payment','$date','$campaign','$status','$amount') ";
    $query = mysqli_query($con,$sql);

    if ($query){
        $_SESSION['success']='Transaction detail added successfully';
        header('location:finance.php');
    }
    else{
        $_SESSION['error']='Transaction detail  not added successfully';
        header('location:finance.php');
    }
}

//add events
if (isset($_POST['addEvent'])){
    $title = $_POST['title'];
    $status = $_POST['status'];
    $desc =$_POST['description'];
    $budget=$_POST['budget'];
    $date =$_POST['date'];
    // Get image name
    $image = $_FILES['image']['name'];

    // echo $title.$status.$desc.$date.$image;
    // die();

   

    // image file directory
    $target = "../images/".basename($image);

    $sql = "INSERT INTO events (`event_id`, `event_title`,`event_info`,`created_on`,`event_image`,`status`,`budget`) 
    VALUES(NULL,'$title','$desc','$date','$image','$status','$budget')";
    $query = mysqli_query($con,$sql);

    if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
        $_SESSION['success'] = "Image uploaded successfully";
        
        // header('location:events.php');
        
    }else {
        $_SESSION['error'] = "Failed to upload image";
        header('location:events.php');
        echo"Image not uploaded successfully";
        die();
    }
    if ($query){
        $_SESSION['success']='Event added successfully';
        
        header('location:events.php');

    }
    else{
        $_SESSION['error']='Event  not added successfully';
        echo mysqli_error($con);
        die();
        header('location:events.php');
    }
}

//edit donation campaign
if (isset($_POST['editEvent'])){
    $id=$_POST['id'];
    $title = $_POST['title'];
    $status = $_POST['status'];
    $budget = $_POST['budget'];
    $desc =$_POST['desc'];
    $date=$_POST['date'];
    // Get image name
    $image = $_FILES['image']['name'];

    // echo $id.$title.$status.$budget.$desc.$image.$date;
    // die();

    // image file directory
    $target = "../images/".basename($image);

    if(!empty($image)){
        $sql = "UPDATE events SET `event_title`= '$title',`event_image`='$image',`event_info` = '$desc',`budget` = '$budget',`status`='$status',`created_on`='$date' WHERE event_id = '$id' ";
        $query = mysqli_query($con,$sql);

        if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
            
            $_SESSION['success'] = "Image uploaded successfully";
            header('location:events.php');
            
        }else {
            $_SESSION['error'] = "Failed to upload image";
            header('location:events.php');
        }
        if ($query){
            $_SESSION['success']='Event edited successfully';
            header('location:events.php');
        }
        else{
            $_SESSION['error']='Event  not edited successfully';
            header('location:events.php');
        }

    }
    else{

    $sql = "UPDATE events SET `event_title`= '$title',`event_info` = '$desc',`budget` = '$budget',`status`='$status',`created_on`='$date' WHERE event_id = '$id' ";
    $query = mysqli_query($con,$sql);

    if ($query){
        $_SESSION['success']='Event edited successfully';
        header('location:events.php');
    }
    else{
        $_SESSION['error']='Event  not edited successfully';
        header('location:events.php');
    }
    }
}
