<?php
session_start();
include"lib/db.php";
include"includes/header.php";
include"includes/sidebar.php";

?>


<!--begin::Wrapper-->
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
    <!--begin::Header-->
    <div id="kt_header" class="header align-items-stretch">
        <!--begin::Brand-->
        <div class="header-brand">
            <!--begin::Logo-->
            <a href="index.php">
                <img alt="Logo" src="../images/dylogo.png" class="h-75px" />
            </a>
            <!--end::Logo-->
        </div>
        <!--end::Brand-->
        <!--begin::Topbar-->
        <div class="topbar">
            <!--begin::Topbar container-->
            <div class="container-fluid py-6 py-lg-0 d-flex flex-column flex-sm-row align-items-lg-stretch justify-content-sm-between">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column me-5">
                    <!--begin::Title-->
                    <h1 class="d-flex flex-column text-dark fw-bolder fs-2 mb-0">Finance</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="index-2.html" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-300 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-dark">Finance</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <!--begin::Action group-->
                <div class="d-flex align-items-center pt-3 pt-sm-0">
                    <!--begin::Action wrapper-->
                    <div class="header-search me-4">
                        <!--begin::Search-->
                        <div id="kt_header_search" class="d-flex align-items-center w-lg-250px" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-search-responsive="lg" data-kt-menu-trigger="auto" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end">
                            <!--begin::Tablet and mobile search toggle-->
                            <div data-kt-search-element="toggle" class="d-flex d-lg-none align-items-center">
                                <div class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                    <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                            </div>
                            <!--end::Tablet and mobile search toggle-->
                            <!--begin::Form(use d-none d-lg-block classes for responsive search)-->
                            <form data-kt-search-element="form" class="d-none d-lg-block w-100 position-relative" autocomplete="off">
                                <!--begin::Hidden input(Added to disable form autocomplete)-->
                                <input type="hidden" />
                                <!--end::Hidden input-->
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
                                <span class="svg-icon svg-icon-3 search-icon position-absolute top-50 translate-middle-y ms-4">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black" />
                                        <path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Input-->
                                <input type="text" class="form-control bg-transparent ps-12" name="search" value="" placeholder="Search" data-kt-search-element="input" />
                                <!--end::Input-->
                                <!--begin::Spinner-->
                                <span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
                                    <span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
                                </span>
                                <!--end::Spinner-->
                                <!--begin::Reset-->
                                <span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-4" data-kt-search-element="clear">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                    <span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </span>
                                <!--end::Reset-->
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--end::Action wrapper-->
                    <!--begin::Actions-->
                    <div class="d-flex">
                        <!--begin::Notifications-->
                        <div class="d-flex align-items-center me-4">
                            <!--begin::Menu- wrapper-->
                            <a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
                                
                                <div class="symbol symbol-40px cursor-pointer" >
                                    <img src="assets/media/avatars/300-1.jpg" alt="" />
                                </div>
                            </a>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column w-250px w-lg-325px" data-kt-menu="true">
                                <!--begin::Heading-->
                                <div class="d-flex flex-column flex-center bgi-no-repeat rounded-top px-9 py-10" style="background-image:url('assets/media/misc/header-dropdown.png')">
                                    <!--begin::Status-->
                                    <span class="badge bg-primary py-2 px-3 mb-3">Hello</span>
                                    <!--end::Status-->
                                    <!--begin::Title-->
                                    <h3 class="text-white fw-bold">Admin</h3>
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                                <!--begin:Nav-->
                                <div class="row g-0">
                                    <!--begin:Item-->
                                    <div class="col-6">
                                        <a href="profile.php" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end border-bottom">
                                            <span class="fs-5 fw-bold text-gray-800 mb-0">Profile</span>
                                            <span class="fs-7 text-gray-400">View</span>
                                        </a>
                                    </div>
                                    <!--end:Item-->
                                    <!--begin:Item-->
                                    <div class="col-6">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#kt_modal_logout" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-bottom">
                                            <span class="fs-5 fw-bold text-gray-800 mb-0">Logout</span>
                                        </a>
                                    </div>
                                    <!--end:Item-->
                                    <!--begin:Item-->
                                </div>
                                <!--end:Nav-->
                            </div>
                            <!--end::Menu-->
                            <!--end::Menu wrapper-->
                        </div>
                        <!--end::Notifications-->
                        <!--begin::Theme mode-->
                        <div class="d-flex align-items-center me-4">
                            <!--begin::Menu toggle-->
                            <a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-muted btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
                                <i class="fonticon-sun fs-2"></i>
                                <i class="fonticon-moon fs-2 d-none"></i>
                            </a>
                            <!--begin::Menu toggle-->
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-muted menu-active-bg menu-state-primary fw-bold py-4 fs-6 w-200px" data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3 my-1">
                                    <a href="index-2.html" class="menu-link px-3 active">
                                        <span class="menu-icon">
                                            <i class="fonticon-sun fs-2"></i>
                                        </span>
                                        <span class="menu-title">Light</span>
                                    </a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3 my-1">
                                    <a href="dark/index.html" class="menu-link px-3">
                                        <span class="menu-icon">
                                            <i class="fonticon-moon fs-2"></i>
                                        </span>
                                        <span class="menu-title">Dark</span>
                                    </a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Theme mode-->
                    </div>
                    <!--end::Actions-->
                </div>
                <!--end::Action group-->
            </div>
            <!--end::Topbar container-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Header-->
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <?php

    if(isset($_SESSION['error'])){
        echo "
          <div class='alert alert-danger text-center'>
          <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p>".$_SESSION['error']."</p> 
          </div>
        ";
        unset($_SESSION['error']);
    }

    if(isset($_SESSION['success'])){
        echo "
          <div class='alert alert-success text-center'>
          <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p>".$_SESSION['success']."</p> 
          </div>
        ";
        unset($_SESSION['success']);
    }
    ?>
            <!--begin::Stats-->
            <div class="row g-6 g-xl-9">
                <div class="col-lg-6 col-xxl-4">
                    <!--begin::Card-->
                    <div class="card h-100">
                        <!--begin::Card body-->
                        <div class="card-body p-9">
                            <?php
                                $n=0;
                                $que = mysqli_query($con, "SELECT * FROM transactions") or die(mysqli_error());
                                while($row = mysqli_fetch_array($que)){
                                    $amount=$row['amount'];
                                    $n+=$amount;
                                }

                                ?>
                            <!--begin::Heading-->
                            <div class="fs-2hx fw-bolder">Ksh <?php echo $n?></div>
                            <div class="fs-4 fw-bold text-gray-400 mb-7">Donations</div>
                            <!--end::Heading-->
                            <!--begin::Wrapper-->
                            <div class="d-flex flex-wrap">
                                <!--begin::Chart-->
                                <div class="d-flex flex-center h-100px w-100px me-9 mb-5">
                                    <canvas id="kt_project_list_chart"></canvas>
                                </div>
                                <!--end::Chart-->
                                <!--begin::Labels-->
                                <div class="d-flex flex-column justify-content-center flex-row-fluid pe-11 mb-5">
                                    <?php
                                    $n=0;
                                    $que = mysqli_query($con, "SELECT * FROM transactions WHERE payment_type>9") or die(mysqli_error());
                                    while($row = mysqli_fetch_array($que)){
                                        $amount=$row['amount'];
                                        $n+=$amount;
                                    }

                                    ?>
                                    <!--begin::Label-->
                                    <div class="d-flex fs-6 fw-bold align-items-center mb-3">
                                        <div class="bullet bg-primary me-3"></div>
                                        <div class="text-gray-400">Paypal</div>
                                        <div class="ms-auto fw-bolder text-gray-700"><?php echo $n?></div>
                                    </div>
                                    <!--end::Label-->
                                    <?php
                                    $n=0;
                                    $que = mysqli_query($con, "SELECT * FROM transactions WHERE payment_type<4") or die(mysqli_error());
                                    while($row = mysqli_fetch_array($que)){
                                        $amount=$row['amount'];
                                        $n+=$amount;
                                    }
                                    ?>
                                   
                                    <!--begin::Label-->
                                    <div class="d-flex fs-6 fw-bold align-items-center mb-3">
                                        <div class="bullet bg-success me-3"></div>
                                        <div class="text-gray-400">Mpesa</div>
                                        <div class="ms-auto fw-bolder text-gray-700"><?php echo $n?></div>
                                    </div>
                                    <!--end::Label-->
                                    <?php
                                    $n=0;
                                    $run_que = mysqli_query($con, "SELECT * FROM transactions WHERE payment_type>4 AND payment_type<7") or die(mysqli_error());
                                    $count = mysqli_num_rows($run_que);
                                    if($count<1){
                                        $n=0;
                                    }
                                    else{
                                        while($row = mysqli_fetch_array($run_que)){
                                            $amount=$row['amount'];
                                            $n+=$amount;
                                        }
                                    }
                                    ?>

                                    <!--begin::Label-->
                                    <div class="d-flex fs-6 fw-bold align-items-center">
                                        <div class="bullet bg-gray-300 me-3"></div>
                                        <div class="text-gray-400">Others</div>
                                        <div class="ms-auto fw-bolder text-gray-700"><?php echo $n?></div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Labels-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!-- <div class="col-lg-6 col-xxl-4">
                    <div class="card h-100">
                        <div class="card-body p-9">
                            <div class="fs-2hx fw-bolder text-success">Ksh 20,820</div>
                            <div class="fs-4 fw-bold text-gray-400 mb-7">Available Balance</div>
                            <div class="fs-6 d-flex justify-content-between mb-4">
                                <div class="fw-bold">Events Budget</div>
                                <div class="d-flex fw-bolder">Ksh 30,000</div>
                            </div>
                            <div class="separator separator-dashed"></div>
                            <div class="fs-6 d-flex justify-content-between my-4">
                                <div class="fw-bold">Balance</div>
                                <div class="d-flex fw-bolder">
                                <span class="svg-icon svg-icon-3 me-1 svg-icon-success">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M13.4 14.8L5.3 6.69999C4.9 6.29999 4.9 5.7 5.3 5.3C5.7 4.9 6.29999 4.9 6.69999 5.3L14.8 13.4L13.4 14.8Z" fill="black" />
                                        <path opacity="0.3" d="M19.8 8.5L8.5 19.8H18.8C19.4 19.8 19.8 19.4 19.8 18.8V8.5Z" fill="black" />
                                    </svg>
                                </span>Ksh 20,820</div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <!--end::Stats-->
            <div class="d-flex flex-wrap flex-stack mb-6 mt-6">
                <!--begin::Title-->
                <h3 class="fw-bolder my-2">Transactions</h3>
                <!--end::Title-->
                <a href="#" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#kt_modal_new_donation">Add Donation</a>
            </div>
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid col-lg-12 mt-5" id="kt_content">
                <!--begin::Container-->
                <div id="kt_content_container" class="">
                    <!--begin::Products-->
                    <div class="card card-flush">
                        <!--begin::Card header-->
                        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <!--begin::Search-->
                                <div class="d-flex align-items-center position-relative my-1">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <input type="text" data-kt-ecommerce-product-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Search Transaction" />
                                </div>
                                <!--end::Search-->
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                                <div class="w-100 mw-150px">
                                    <!--begin::Select2-->
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="PaymentMode" data-kt-ecommerce-product-filter="status">
                                        <option></option>
                                        <option value="all">All</option>
                                        <option value="mpesa">Mpesa</option>
                                        <option value="paypal">Paypal</option>
                                        <option value="others">Others</option>
                                    </select>
                                    <!--end::Select2-->
                                </div>
                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Table-->
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_ecommerce_products_table">
                                <!--begin::Table head-->
                                <thead>
                                    <!--begin::Table row-->
                                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                        <th class="w-10px pe-2">
                                            <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_products_table .form-check-input" value="1" />
                                            </div>
                                        </th>
                                        <th class="min-w-100px">Transaction ID</th>
                                        <th class="text-end min-w-100px">Account</th>
                                        <th class="text-end min-w-100px">Amount</th>
                                        <th class="text-end min-w-100px">Date</th>
                                        <th class="text-end min-w-100px">Campaign</th>
                                        <th class="text-end min-w-100px">Payment</th>
                                        <th class="text-end min-w-100px">Status</th>
                                    </tr>
                                    <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fw-bold text-gray-600">
                                    <!--begin::Table row-->
                                    <?php
                                    $n=0;
                                    $n++;
                                    $query = mysqli_query($con, "SELECT * FROM transactions") or die(mysqli_error());
                                    while($fetch = mysqli_fetch_array($query)){
                                        $date = $fetch['trans_date'];
                                        $re=date_create($date);

                                        $pay=$fetch['payment_type'];
                                        $status=$fetch['status'];
                                        if($pay<4){
                                            $payment="mpesa";
                                        }
                                        if($pay==5){
                                            $payment="others";
                                        }
                                        if($pay>9){
                                            $payment="paypal";
                                        }
                                        if($status<2){
                                            $stat="<div class='badge badge-light-warning'>Pending</div>";
                                        }else{
                                            $stat="<div class='badge badge-light-success'>Confirmed</div>";
                                        }
                                    ?>
                                    <tr>
                                        <!--begin::Checkbox-->
                                        <td>
                                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input" type="checkbox" value="1" />
                                            </div>
                                        </td>
                                        <!--end::Checkbox-->
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="ms-5">
                                                    <!--begin::Title-->
                                                    <span class="text-gray-800 text-hover-primary fs-5 fw-bolder"><?php echo $fetch['transactionId']?></a>
                                                    <!--end::Title-->
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end pe-0">
                                            <span class="fw-bolder"><?php echo $fetch['trans_member']?></span>
                                        </td>
                                        <td class="text-end pe-0" >
                                            <span class="fw-bolder">Ksh <?php echo $fetch['amount']?></span>
                                        </td>
                                        <td class="text-end pe-0">
                                            <span class="fw-bolder text-dark"><?php echo date_format($re,"l F d, Y") ?></span>
                                        </td>
                                        <td class="text-end pe-0">
                                            <?php 
                                            $camp=$fetch['donation_campaign'];
                                            $que_run = mysqli_query($con, "SELECT * FROM donation WHERE donation_id=$camp") or die(mysqli_error());
                                            $fetch = mysqli_fetch_array($que_run);
                                            $camp_name=$fetch['donation_title'];

                                            ?>
                                            <span class="fw-bolder text-dark"><?php echo $camp_name
                                            ?>
                                            </span>
                                        </td>
                                        <td class="text-end pe-0" data-order="<?php echo $payment?>">
                                            <!--begin::Badges-->
                                            <div class="badge badge-light-primary"><?php echo $payment?></div>
                                            <!--end::Badges-->
                                        </td>
                                        <td class="text-end">
                                            <?php echo $stat?>
                                        </td>
                                    </tr>
                                    <!--end::Table row-->
                                    <?php
                                    }
                                    ?>
                                </tbody>
                                <!--end::Table body-->
                            </table>
                            <!--end::Table-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Content-->
            <!--begin::Modals-->
            <!--begin::Modal - Add donation-->
            <div class="modal fade" id="kt_modal_new_donation" tabindex="-1" aria-hidden="true">
                <!--begin::Modal dialog-->
                <div class="modal-dialog modal-dialog-centered mw-750px">
                    <!--begin::Modal content-->
                    <div class="modal-content rounded">
                        <!--begin::Modal header-->
                        <div class="modal-header pb-0 border-0 justify-content-end">
                            <!--begin::Close-->
                            <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </div>
                            <!--end::Close-->
                        </div>
                        <!--begin::Modal header-->
                        <!--begin::Modal body-->
                        <div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
                            <!--begin:Form-->
                            <form id="" class="form" action="action.php" method="post">
                                <!--begin::Heading-->
                                <div class="mb-13 text-center">
                                    <!--begin::Title-->
                                    <h1 class="mb-3">Add Donation</h1>
                                    <!--end::Title-->
                                    <!--begin::Description-->
                                    <div class="text-gray-400 fw-bold fs-5">
                                        Account for donations received personally.
                                    </div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-8 fv-row">
                                    <!--begin::Label-->
                                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                        <span class="required">From</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Enter details for donator"></i>
                                    </label>
                                    <!--end::Label-->
                                    <input type="text" class="form-control form-control-solid" placeholder="Enter email or names or number" name="user" />
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-8 fv-row">
                                    <!--begin::Label-->
                                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                        <span class="required">Payment type</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Choose Payment type"></i>
                                    </label>
                                    <!--end::Label-->
                                    <select class="form-select form-select-solid" name="pay">
                                        <option value="1">Mpesa</option>
                                        <option value="10">Paypal</option>
                                        <option value="5">Other</option>
                                    </select>
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-8 fv-row">
                                    <!--begin::Label-->
                                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                        <span class="required">Transaction ID</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Enter transaction ID"></i>
                                    </label>
                                    <!--end::Label-->
                                    <input type="text" class="form-control form-control-solid" placeholder="Enter transaction Id" name="trans" />
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-8 fv-row">
                                    <!--begin::Label-->
                                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                        <span class="required">Amount</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Enter Amount donated"></i>
                                    </label>
                                    <!--end::Label-->
                                    <input type="text" class="form-control form-control-solid" placeholder="Enter amount donated" name="amount" />
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-8 fv-row">
                                    <!--begin::Label-->
                                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                        <span class="required">Donation campaign</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Choose donation campaign"></i>
                                    </label>
                                    <!--end::Label-->
                                    <select class="form-select form-select-solid" name="campaign">
                                        <?php
                                        $que = mysqli_query($con, "SELECT * FROM donation") or die(mysqli_error());
                                        while($row = mysqli_fetch_array($que)){
                                            $donId=$row['donation_id'];
                                            $title=$row['donation_title'];
                                        ?>

                                        <option value="<?php echo $donId?>"><?php echo $title?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="row g-9 mb-8">
                                    <!--begin::Col-->
                                    <div class="col-md-6 fv-row">
                                        <label class="required fs-6 fw-bold mb-2">Date</label>
                                        <!--begin::Input-->
                                        <div class="position-relative d-flex align-items-center">
                                            <!--begin::Icon-->
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
                                                            <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
                                                            <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
                                                            <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
                                                        </svg>
                                                    </span>
                                                    <!--end::Svg Icon-->
                                                </span>
                                            </div>
                                            <!--end::Icon-->
                                            <!--begin::Datepicker-->
                                            <input type="date" class="form-control form-control-solid ps-12" placeholder="Select a date" name="date" />
                                            <!--end::Datepicker-->
                                        </div>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-md-6 fv-row">
                                        <label class="required fs-6 fw-bold mb-2">Status</label>
                                        <select class="form-select form-select-solid" data-control="select2" data-placeholder="Confirmed" data-hide-search="true" name="status">
                                            <option value="5" selected="selected">Confirmed</option>
                                            <option value="1">Pending</option>
                                        </select>
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Actions-->
                                <div class="text-center">
                                    <button type="reset" id="kt_modal_new_ticket_cancel" class="btn btn-light me-3">Cancel</button>
                                    <button type="submit" name="transaction" id="" class="btn btn-primary">
                                        <span class="indicator-label">Submit</span>
                                        <!-- <span class="indicator-progress">Please wait... 
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span> -->
                                    </button>
                                </div>
                                <!--end::Actions-->
                            </form>
                            <!--end:Form-->
                        </div>
                        <!--end::Modal body-->
                    </div>
                    <!--end::Modal content-->
                </div>
                <!--end::Modal dialog-->
            </div>
            <!--end::Modal - Add donation-->
            <!--end::Modals-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Content-->
    <!--begin::Footer-->
    <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
         <!--begin::Container-->
         <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
            <!--begin::Copyright-->
            <div class="text-dark order-2 order-md-1">
                <img alt="Logo" src="../images/favicon.png" class="h-80px "  />
                <span class="text-muted fw-bold me-1">© Duara Yangu Foundation <script>document.write(new Date().getFullYear());</script> | Designed by IndigoRegime</span>
            </div>
            <!--end::Copyright-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Footer-->
</div>
<!--end::Wrapper-->


<?php
include"includes/footer.php";