</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->

		<!--begin::Modal - Logout-->
		<div class="modal fade" id="kt_modal_logout" tabindex="-1" aria-hidden="true">
                <!--begin::Modal dialog-->
                <div class="modal-dialog modal-dialog-centered mw-750px">
                    <!--begin::Modal content-->
                    <div class="modal-content rounded">
                        <!--begin::Modal header-->
                        <div class="modal-header pb-0 border-0 justify-content-end">
                            <!--begin::Close-->
                            <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </div>
                            <!--end::Close-->
                        </div>
                        <!--begin::Modal header-->
                        <!--begin::Modal body-->
                        <div class="modal-body scroll-y px-10 px-lg-10 pt-0 pb-10">
                            <!--begin:Form-->
                            <form id="" class="form" action="action.php" method="post">
                                <!--begin::Heading-->
                                <div class="mb-5s text-center">
                                    <!--begin::Title-->
                                    <h1 class="mb-5">Logout</h1>
                                    <!--end::Title-->
                                    <!--begin::Description-->
                                    <div class="text-gray-400 fw-bold fs-5">
                                        Are you sure you want to logout?
                                    </div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Actions-->
                                <div class="text-center mt-5">
                                    <button type="reset" id="kt_modal_new_ticket_cancel" class="btn btn-light me-3">Cancel</button>
                                    <button type="submit" id="kt_modal_new_ticket_submit" class="btn btn-primary" name="logout">
                                        <span class="indicator-label">Logout</span>
                                        <span class="indicator-progress">Please wait... 
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                                <!--end::Actions-->
                            </form>
                            <!--end:Form-->
                        </div>
                        <!--end::Modal body-->
                    </div>
                    <!--end::Modal content-->
                </div>
                <!--end::Modal dialog-->
            </div>
            <!--end::Modal - Logout-->

		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--begin::Javascript-->
		<script>var hostUrl = "../assets/index.html";</script>
		<!--begin::Global Javascript Bundle-->
		<script src="../assets/plugins/global/plugins.bundle.js"></script>
		<script src="../assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript-->
		<script src="../assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<script src="../assets/plugins/custom/vis-timeline/vis-timeline.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript-->
        <script src="../assets/js/custom/apps/ecommerce/catalog/save-product.js"></script>
        <script src="../assets/js/custom/apps/customers/list/export.js"></script>
		<script src="../assets/js/custom/apps/customers/list/list.js"></script>
		<script src="../assets/js/custom/apps/customers/add.js"></script>
        <script src="../assets/js/custom/apps/projects/list/list.js"></script>
        <script src="../assets/js/custom/apps/ecommerce/catalog/products.js"></script>
		<script src="../assets/js/custom/apps/support-center/tickets/create.js"></script>
		<script src="../assets/js/custom/documentation/documentation.js"></script>
		<script src="../assets/js/widgets.bundle.js"></script>
		<script src="../assets/js/custom/widgets.js"></script>
		<script src="../assets/js/custom/apps/chat/chat.js"></script>
		<script src="../assets/js/custom/intro.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/type.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/budget.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/settings.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/team.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/targets.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/files.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/complete.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-project/main.js"></script>
		<script src="../assets/js/custom/utilities/modals/upgrade-plan.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-app.js"></script>
		<script src="../assets/js/custom/utilities/modals/create-campaign.js"></script>
		<script src="../assets/js/custom/utilities/modals/users-search.js"></script>
		<!--end::Page Custom Javascript-->
        <!--begin::Page Custom Javascript(used by this page)-->
		<script src="../assets/js/custom/apps/user-management/users/list/table.js"></script>
		<script src="../assets/js/custom/apps/user-management/users/list/export-users.js"></script>
		<script src="../assets/js/custom/apps/user-management/users/list/add.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->

</html>