<?php
session_start();

if (!isset($_SESSION['aid'])){
    header('location:login.php');
}
include"lib/db.php";
include"includes/header.php";
include"includes/sidebar.php";

$view=$_GET['view'];


if($view=='donation'){
	$did=$_GET['id'];

$query = mysqli_query($con, "SELECT * FROM donation WHERE donation_id= $did") or die(mysqli_error());
$fetch = mysqli_fetch_array($query);
$image=$fetch['donation_image'];
$status=$fetch['status'];
if($status<2){
	$stat="<span class='badge badge-light-warning me-auto'>Draft</span>";
}else{
	$stat="<span class='badge badge-light-success me-auto'>Approved</span>";
}



?>

<!--begin::Wrapper-->
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header align-items-stretch">
						<!--begin::Brand-->
						<div class="header-brand">
							 <!--begin::Logo-->
							 <a href="index.php">
								<img alt="Logo" src="../images/dylogo.png" class="h-75px" />
							</a>
							<!--end::Logo-->
						</div>
						<!--end::Brand-->
						<!--begin::Topbar-->
						<div class="topbar">
							<!--begin::Topbar container-->
							<div class="container-fluid py-6 py-lg-0 d-flex flex-column flex-sm-row align-items-lg-stretch justify-content-sm-between">
								<!--begin::Page title-->
								<div class="page-title d-flex flex-column me-5">
									<!--begin::Title-->
									<h1 class="d-flex flex-column text-dark fw-bolder fs-2 mb-0">View Donation campaign</h1>
									<!--end::Title-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="index.php" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-300 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted"><a href="donation.php">Donation campaign list</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-300 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">View </li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
                                <!--begin::Action group-->
								<div class="d-flex align-items-center pt-3 pt-sm-0">
									<!--begin::Action wrapper-->
									<div class="header-search me-4">
										<!--begin::Search-->
										<div id="kt_header_search" class="d-flex align-items-center w-lg-250px" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-search-responsive="lg" data-kt-menu-trigger="auto" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end">
											<!--begin::Tablet and mobile search toggle-->
											<div data-kt-search-element="toggle" class="d-flex d-lg-none align-items-center">
												<div class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary">
													<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
													<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
															<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</div>
											</div>
											<!--end::Tablet and mobile search toggle-->
											<!--begin::Form(use d-none d-lg-block classes for responsive search)-->
											<form data-kt-search-element="form" class="d-none d-lg-block w-100 position-relative" autocomplete="off">
												<!--begin::Hidden input(Added to disable form autocomplete)-->
												<input type="hidden" />
												<!--end::Hidden input-->
												<!--begin::Icon-->
												<!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
												<span class="svg-icon svg-icon-3 search-icon position-absolute top-50 translate-middle-y ms-4">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black" />
														<path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<!--end::Icon-->
												<!--begin::Input-->
												<input type="text" class="form-control bg-transparent ps-12" name="search" value="" placeholder="Search" data-kt-search-element="input" />
												<!--end::Input-->
												<!--begin::Spinner-->
												<span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
													<span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
												</span>
												<!--end::Spinner-->
												<!--begin::Reset-->
												<span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-4" data-kt-search-element="clear">
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
													<span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
															<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</span>
												<!--end::Reset-->
											</form>
											<!--end::Form-->
										</div>
										<!--end::Search-->
									</div>
									<!--end::Action wrapper-->
									<!--begin::Actions-->
									<div class="d-flex">
										<!--begin::Notifications-->
										<div class="d-flex align-items-center me-4">
											<!--begin::Menu- wrapper-->
											<a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												
												<div class="symbol symbol-40px cursor-pointer" >
													<img src="assets/media/avatars/300-1.jpg" alt="" />
												</div>
											</a>
											<!--begin::Menu-->
											<div class="menu menu-sub menu-sub-dropdown menu-column w-250px w-lg-325px" data-kt-menu="true">
												<!--begin::Heading-->
												<div class="d-flex flex-column flex-center bgi-no-repeat rounded-top px-9 py-10" style="background-image:url('assets/media/misc/header-dropdown.png')">
													<!--begin::Status-->
													<span class="badge bg-primary py-2 px-3 mb-3">Hello</span>
													<!--end::Status-->
													<!--begin::Title-->
													<h3 class="text-white fw-bold">Admin</h3>
													<!--end::Title-->
												</div>
												<!--end::Heading-->
												<!--begin:Nav-->
												<div class="row g-0">
													<!--begin:Item-->
													<div class="col-6">
														<a href="profile.php" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end border-bottom">
															<span class="fs-5 fw-bold text-gray-800 mb-0">Profile</span>
															<span class="fs-7 text-gray-400">View</span>
														</a>
													</div>
													<!--end:Item-->
													<!--begin:Item-->
													<div class="col-6">
														<a href="" data-bs-toggle="modal" data-bs-target="#kt_modal_logout" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-bottom">
															<span class="fs-5 fw-bold text-gray-800 mb-0">Logout</span>
														</a>
													</div>
													<!--end:Item-->
													<!--begin:Item-->
												</div>
												<!--end:Nav-->
											</div>
											<!--end::Menu-->
											<!--end::Menu wrapper-->
										</div>
										<!--end::Notifications-->
										<!--begin::Theme mode-->
										<div class="d-flex align-items-center me-4">
											<!--begin::Menu toggle-->
											<a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-muted btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												<i class="fonticon-sun fs-2"></i>
												<i class="fonticon-moon fs-2 d-none"></i>
											</a>
											<!--begin::Menu toggle-->
											<!--begin::Menu-->
											<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-muted menu-active-bg menu-state-primary fw-bold py-4 fs-6 w-200px" data-kt-menu="true">
												<!--begin::Menu item-->
												<div class="menu-item px-3 my-1">
													<a href="index-2.html" class="menu-link px-3 active">
														<span class="menu-icon">
															<i class="fonticon-sun fs-2"></i>
														</span>
														<span class="menu-title">Light</span>
													</a>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu item-->
												<div class="menu-item px-3 my-1">
													<a href="dark/index.html" class="menu-link px-3">
														<span class="menu-icon">
															<i class="fonticon-moon fs-2"></i>
														</span>
														<span class="menu-title">Dark</span>
													</a>
												</div>
												<!--end::Menu item-->
											</div>
											<!--end::Menu-->
										</div>
										<!--end::Theme mode-->
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Action group-->
							</div>
							<!--end::Topbar container-->
						</div>
						<!--end::Topbar-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluid">
							<!--begin::Navbar-->
							<div class="card mb-6 mb-xl-9">
								<div class="card-body pt-9 pb-0">
									<!--begin::Details-->
									<div class="d-flex flex-wrap flex-sm-nowrap mb-6">
										<!--begin::Image-->
										<div class="d-flex flex-center flex-shrink-0 bg-light rounded w-100px h-100px w-lg-150px h-lg-150px me-7 mb-4" >
											<img class="mw-200px mw-lg-150px" src="../images/<?php echo $fetch['donation_image']?>" alt="image" />
										</div>
										<!--end::Image-->
										<!--begin::Wrapper-->
										<div class="flex-grow-1">
											<!--begin::Head-->
											<div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
												<!--begin::Details-->
												<div class="d-flex flex-column">
													<!--begin::Status-->
													<div class="d-flex align-items-center mb-1">
														<a href="#" class="text-gray-800 text-hover-primary fs-2 fw-bolder me-3"><?php echo $fetch['donation_title']?></a>
														<?php echo $stat?>
													</div>
													<!--end::Status-->
													<!--begin::Description-->
													<div class="d-flex flex-wrap fw-bold mb-4 fs-5 text-gray-400">
                                                        <?php echo $fetch['donation_info']?>
                                                    </div>
													<!--end::Description-->
												</div>
												<!--end::Details-->
												<!--end::Actions-->
											</div>
											<!--end::Head-->
											<!--begin::Info-->
											<div class="d-flex flex-wrap justify-content-start">
												<!--begin::Stats-->
												<div class="d-flex flex-wrap">
													<!--begin::Stat
													<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
														
														<div class="d-flex align-items-center">
															<div class="fs-4 fw-bolder">29 Jan, 2022</div>
														</div>
														<div class="fw-bold fs-6 text-gray-400">Event Date</div>
													</div>
													end::Stat-->
													<!--begin::Stat-->
													<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
														<!--begin::Number-->
														<div class="d-flex align-items-center">
															<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
															<span class="svg-icon svg-icon-3 svg-icon-success me-2">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
																	<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
																</svg>
															</span>
															<!--end::Svg Icon-->
															<div class="fs-4 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $fetch['donation_target']?>" data-kt-countup-prefix="Ksh">0</div>
														</div>
														<!--end::Number-->
														<!--begin::Label-->
														<div class="fw-bold fs-6 text-gray-400">Target Amount</div>
														<!--end::Label-->
													</div>
													<!--end::Stat-->
													<?php
													$n=0;
													$que = mysqli_query($con, "SELECT * FROM transactions WHERE donation_campaign=$did") or die(mysqli_error());
													while($row = mysqli_fetch_array($que)){
														$amount=$row['amount'];
														$n+=$amount;
													}
													?>

													<!--begin::Stat-->
													<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
														<!--begin::Number-->
														<div class="d-flex align-items-center">
															<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
															<span class="svg-icon svg-icon-3 svg-icon-success me-2">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
																	<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
																</svg>
															</span>
															<!--end::Svg Icon-->
															<div class="fs-4 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $n?>" data-kt-countup-prefix="Ksh">0</div>
														</div>
														<!--end::Number-->
														<!--begin::Label-->
														<div class="fw-bold fs-6 text-gray-400">Donated  Amount</div>
														<!--end::Label-->
													</div>
													<!--end::Stat-->
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
                                            <!--begin::Actions-->
												<div class="d-flex mb-4">
													<a href="edit_donation.php?id=<?php echo $fetch['donation_id']?>" class="btn btn-sm btn-primary me-3">Edit </a>
												</div>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Details-->
									<div class="separator"></div>
								</div>
							</div>
							<!--end::Navbar-->
							
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						 <!--begin::Container-->
						 <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<img alt="Logo" src="../images/favicon.png" class="h-80px "  />
								<span class="text-muted fw-bold me-1">© Duara Yangu Foundation <script>document.write(new Date().getFullYear());</script> | Designed by IndigoRegime</span>
							</div>
							<!--end::Copyright-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
<?php
}

if($view=='event'){
	$eid=$_GET['id'];

	$query = mysqli_query($con, "SELECT * FROM events WHERE event_id= $eid") or die(mysqli_error());
	$fetch = mysqli_fetch_array($query);
	$image=$fetch['event_image'];
	$status=$fetch['status'];
	if($status<2){
		$stat="<span class='badge badge-light-success me-auto'>Approved</span>";
		
	}else{
		$stat="<span class='badge badge-light-warning me-auto'>Draft</span>";
	}
	

?>

<!--begin::Wrapper-->
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header align-items-stretch">
						<!--begin::Brand-->
						<div class="header-brand">
							 <!--begin::Logo-->
							 <a href="index.php">
								<img alt="Logo" src="../images/dylogo.png" class="h-75px" />
							</a>
							<!--end::Logo-->
						</div>
						<!--end::Brand-->
						<!--begin::Topbar-->
						<div class="topbar">
							<!--begin::Topbar container-->
							<div class="container-fluid py-6 py-lg-0 d-flex flex-column flex-sm-row align-items-lg-stretch justify-content-sm-between">
								<!--begin::Page title-->
								<div class="page-title d-flex flex-column me-5">
									<!--begin::Title-->
									<h1 class="d-flex flex-column text-dark fw-bolder fs-2 mb-0">View Event </h1>
									<!--end::Title-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="index.php" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-300 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted"><a href="event.php">Event List</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-300 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">View </li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
                                <!--begin::Action group-->
								<div class="d-flex align-items-center pt-3 pt-sm-0">
									<!--begin::Action wrapper-->
									<div class="header-search me-4">
										<!--begin::Search-->
										<div id="kt_header_search" class="d-flex align-items-center w-lg-250px" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-search-responsive="lg" data-kt-menu-trigger="auto" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end">
											<!--begin::Tablet and mobile search toggle-->
											<div data-kt-search-element="toggle" class="d-flex d-lg-none align-items-center">
												<div class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary">
													<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
													<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
															<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</div>
											</div>
											<!--end::Tablet and mobile search toggle-->
											<!--begin::Form(use d-none d-lg-block classes for responsive search)-->
											<form data-kt-search-element="form" class="d-none d-lg-block w-100 position-relative" autocomplete="off">
												<!--begin::Hidden input(Added to disable form autocomplete)-->
												<input type="hidden" />
												<!--end::Hidden input-->
												<!--begin::Icon-->
												<!--begin::Svg Icon | path: icons/duotune/general/gen004.svg-->
												<span class="svg-icon svg-icon-3 search-icon position-absolute top-50 translate-middle-y ms-4">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black" />
														<path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<!--end::Icon-->
												<!--begin::Input-->
												<input type="text" class="form-control bg-transparent ps-12" name="search" value="" placeholder="Search" data-kt-search-element="input" />
												<!--end::Input-->
												<!--begin::Spinner-->
												<span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
													<span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
												</span>
												<!--end::Spinner-->
												<!--begin::Reset-->
												<span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-4" data-kt-search-element="clear">
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
													<span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
															<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</span>
												<!--end::Reset-->
											</form>
											<!--end::Form-->
										</div>
										<!--end::Search-->
									</div>
									<!--end::Action wrapper-->
									<!--begin::Actions-->
									<div class="d-flex">
										<!--begin::Notifications-->
										<div class="d-flex align-items-center me-4">
											<!--begin::Menu- wrapper-->
											<a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-700 btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												
												<div class="symbol symbol-40px cursor-pointer" >
													<img src="assets/media/avatars/300-1.jpg" alt="" />
												</div>
											</a>
											<!--begin::Menu-->
											<div class="menu menu-sub menu-sub-dropdown menu-column w-250px w-lg-325px" data-kt-menu="true">
												<!--begin::Heading-->
												<div class="d-flex flex-column flex-center bgi-no-repeat rounded-top px-9 py-10" style="background-image:url('assets/media/misc/header-dropdown.png')">
													<!--begin::Status-->
													<span class="badge bg-primary py-2 px-3 mb-3">Hello</span>
													<!--end::Status-->
													<!--begin::Title-->
													<h3 class="text-white fw-bold">Admin</h3>
													<!--end::Title-->
												</div>
												<!--end::Heading-->
												<!--begin:Nav-->
												<div class="row g-0">
													<!--begin:Item-->
													<div class="col-6">
														<a href="profile.php" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end border-bottom">
															<span class="fs-5 fw-bold text-gray-800 mb-0">Profile</span>
															<span class="fs-7 text-gray-400">View</span>
														</a>
													</div>
													<!--end:Item-->
													<!--begin:Item-->
													<div class="col-6">
														<a href="" data-bs-toggle="modal" data-bs-target="#kt_modal_logout" class="d-flex flex-column flex-center h-100 p-6 bg-hover-light border-bottom">
															<span class="fs-5 fw-bold text-gray-800 mb-0">Logout</span>
														</a>
													</div>
													<!--end:Item-->
													<!--begin:Item-->
												</div>
												<!--end:Nav-->
											</div>
											<!--end::Menu-->
											<!--end::Menu wrapper-->
										</div>
										<!--end::Notifications-->
										<!--begin::Theme mode-->
										<div class="d-flex align-items-center me-4">
											<!--begin::Menu toggle-->
											<a href="#" class="btn btn-icon btn-active-light btn-outline btn-outline-default btn-icon-gray-muted btn-active-icon-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												<i class="fonticon-sun fs-2"></i>
												<i class="fonticon-moon fs-2 d-none"></i>
											</a>
											<!--begin::Menu toggle-->
											<!--begin::Menu-->
											<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-muted menu-active-bg menu-state-primary fw-bold py-4 fs-6 w-200px" data-kt-menu="true">
												<!--begin::Menu item-->
												<div class="menu-item px-3 my-1">
													<a href="index-2.html" class="menu-link px-3 active">
														<span class="menu-icon">
															<i class="fonticon-sun fs-2"></i>
														</span>
														<span class="menu-title">Light</span>
													</a>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu item-->
												<div class="menu-item px-3 my-1">
													<a href="dark/index.html" class="menu-link px-3">
														<span class="menu-icon">
															<i class="fonticon-moon fs-2"></i>
														</span>
														<span class="menu-title">Dark</span>
													</a>
												</div>
												<!--end::Menu item-->
											</div>
											<!--end::Menu-->
										</div>
										<!--end::Theme mode-->
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Action group-->
							</div>
							<!--end::Topbar container-->
						</div>
						<!--end::Topbar-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluid">
							<!--begin::Navbar-->
							<div class="card mb-6 mb-xl-9">
								<div class="card-body pt-9 pb-0">
									<!--begin::Details-->
									<div class="d-flex flex-wrap flex-sm-nowrap mb-6">
										<!--begin::Image-->
										<div class="d-flex flex-center flex-shrink-0 bg-light rounded w-100px h-100px w-lg-150px h-lg-150px me-7 mb-4" >
											<img class="mw-200px mw-lg-150px" src="../images/<?php echo $fetch['event_image']?>" alt="image" />
										</div>
										<!--end::Image-->
										<!--begin::Wrapper-->
										<div class="flex-grow-1">
											<!--begin::Head-->
											<div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
												<!--begin::Details-->
												<div class="d-flex flex-column">
													<!--begin::Status-->
													<div class="d-flex align-items-center mb-1">
														<a href="#" class="text-gray-800 text-hover-primary fs-2 fw-bolder me-3"><?php echo $fetch['event_title']?></a>
														<?php echo $stat?>
													</div>
													<!--end::Status-->
													<!--begin::Description-->
													<div class="d-flex flex-wrap fw-bold mb-4 fs-5 text-gray-400">
                                                        <?php echo $fetch['event_info']?>
                                                    </div>
													<!--end::Description-->
												</div>
												<!--end::Details-->
												<!--end::Actions-->
											</div>
											<!--end::Head-->
											<!--begin::Info-->
											<div class="d-flex flex-wrap justify-content-start">
												<!--begin::Stats-->
												<div class="d-flex flex-wrap">
													<!--begin::Stat
													<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
														
														<div class="d-flex align-items-center">
															<div class="fs-4 fw-bolder">29 Jan, 2022</div>
														</div>
														<div class="fw-bold fs-6 text-gray-400">Event Date</div>
													</div>
													end::Stat-->
													<!--begin::Stat-->
													<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
														<!--begin::Number-->
														<div class="d-flex align-items-center">
															<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
															<span class="svg-icon svg-icon-3 svg-icon-success me-2">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
																	<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
																</svg>
															</span>
															<!--end::Svg Icon-->
															<div class="fs-4 fw-bolder" data-kt-countup="true" data-kt-countup-value="<?php echo $fetch['budget']?>" data-kt-countup-prefix="Ksh">0</div>
														</div>
														<!--end::Number-->
														<!--begin::Label-->
														<div class="fw-bold fs-6 text-gray-400">Expense</div>
														<!--end::Label-->
													</div>
													<!--end::Stat-->
													

												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
                                            <!--begin::Actions-->
												<div class="d-flex mb-4">
													<a href="edit.php?id=<?php echo $fetch['event_id']?>" class="btn btn-sm btn-primary me-3">Edit </a>
												</div>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Details-->
									<div class="separator"></div>
								</div>
							</div>
							<!--end::Navbar-->
							
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						 <!--begin::Container-->
						 <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<img alt="Logo" src="../images/favicon.png" class="h-80px "  />
								<span class="text-muted fw-bold me-1">© Duara Yangu Foundation <script>document.write(new Date().getFullYear());</script> | Designed by IndigoRegime</span>
							</div>
							<!--end::Copyright-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->


<?php
}
include"includes/footer.php";
?>